package com.heima.model.article.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章信息表，存储已发布的文章
 *
 * @TableName ap_article
 */
@TableName(value = "ap_article")
@Data
public class ApArticle implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(type = IdType.ID_WORKER)
    private Long id;
    /**
     * 文章作者的ID
     */
    private Integer authorId;
    /**
     * 来源
     */
    private Boolean origin;
    /**
     * 省市
     */
    private Integer provinceId;
    /**
     * 收藏数量
     */
    private Integer collection;
    /**
     * 文章图片
     * 多张逗号分隔
     */
    private String images;
    /**
     * 作者昵称
     */
    private String authorName;
    /**
     * 发布时间
     */
    private Date publishTime;
    /**
     * 阅读数量
     */
    private Integer views;
    /**
     * 评论数量
     */
    private Integer comment;
    /**
     * 点赞数量
     */
    private Integer likes;
    /**
     * 文章布局
     * 0 无图文章
     * 1 单图文章
     * 2 多图文章
     */
    private Byte layout;
    /**
     * 频道名称
     */
    private String channelName;
    /**
     * 同步状态
     */
    private Boolean syncStatus;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 区县
     */
    private Integer countyId;
    /**
     * 市区
     */
    private Integer cityId;
    /**
     * 文章标签最多3个 逗号分隔
     */
    private String labels;
    /**
     * 文章标记
     * 0 普通文章
     * 1 热点文章
     * 2 置顶文章
     * 3 精品文章
     * 4 大V 文章
     */
    private Byte flag;
    /**
     * 文章所属频道ID
     */
    private Integer channelId;
    /**
     * 标题
     */
    private String title;
}