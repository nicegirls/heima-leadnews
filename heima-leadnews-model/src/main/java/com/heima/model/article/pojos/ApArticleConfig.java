package com.heima.model.article.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * APP已发布文章配置表
 *
 * @TableName ap_article_config
 */
@TableName(value = "ap_article_config")
@Data
public class ApArticleConfig implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.ID_WORKER)
    private Long id;
    /**
     * 是否已删除
     */
    private Boolean isDelete;
    /**
     * 是否可评论
     */
    private Boolean isComment;
    /**
     * 文章ID
     */
    private Long articleId;
    /**
     * 是否转发
     */
    private Boolean isForward;
    /**
     * 是否下架
     */
    private Boolean isDown;
}