package com.heima.model.article.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * APP收藏信息表
 * @TableName ap_collection
 */
@TableName(value ="ap_collection")
@Data
public class ApCollection implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    /**
     * 发布时间
     */
    private Date publishedTime;
    /**
     * 创建时间
     */
    private Date collectionTime;
    /**
     * 点赞内容类型
            0文章
            1动态
     */
    private short type;
    /**
     * 文章ID
     */
    private Long articleId;
    /**
     * 实体ID
     */
    private Integer entryId;
}