package com.heima.model.user.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author 韦东
 * @date 2021/9/2 周四
 */
@Data
public class AuthDTO extends PageRequestDto {
    private static final long serialVersionUID = 4023859107350945489L;
    /**
     * 状态
     */
    private Byte status;

    /**
     * id
     */
    private Integer id;
    /**
     * 驳回原因
     */
    private String msg;
}
