package com.heima.model.admin.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 频道信息表
 *
 * @author 韦东
 * @date 2021-08-30
 */
@TableName(value ="ad_channel")
@Data
public class AdChannel implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 频道名称
     */
    private String name;
    /**
     * 频道描述
     */
    private String description;
    /**
     * 是否默认频道
     */
    private Boolean isDefault;
    /**
     *
     */
    private Boolean status;
    /**
     * 默认排序
     */
    private Byte ord;
    /**
     * 创建时间
     */
    private Date createdTime;
}