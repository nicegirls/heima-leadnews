package com.heima.model.admin.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author 韦东
 * @date 2021/9/1 周三
 */
@Data
public class SensitiveDto extends PageRequestDto {
    private String name;
}

