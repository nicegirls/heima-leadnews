package com.heima.model.admin.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 敏感词信息表
 *
 * @author 韦东
 * @date 2021-09-01
 */
@TableName(value ="ad_sensitive")
@Data
public class AdSensitive implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 敏感词
     */
    private String sensitives;
    /**
     * 创建时间
     */
    private Date createdTime;
}