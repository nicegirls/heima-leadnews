package com.heima.model.wemedia.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.Date;

/**
 * 自媒体图文内容信息表
 *
 * @TableName wm_news
 */
@TableName(value = "wm_news")
@Data
public class WmNews implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 标题
     */
    private String title;
    /**
     * 标签
     */
    private String labels;
    /**
     * 定时发布时间，不定时则为空
     */
    private Date publishTime;
    /**
     * 图文频道ID
     */
    private Integer channelId;
    /**
     * 文章布局
     * 0 无图文章
     * 1 单图文章
     * 3 多图文章
     */
    private Byte type;
    /**
     * 拒绝理由
     */
    private String reason;
    /**
     * 自媒体用户ID
     */
    private Integer userId;
    /**
     * 是否上架
     */
    private Byte enable;
    /**
     * 图片用逗号分隔
     */
    private String images;
    /**
     * 发布库文章ID
     */
    private Long articleId;
    /**
     * 图文内容
     */
    private String content;
    /**
     * 当前状态
     * 0 草稿
     * 1 提交（待审核）
     * 2 审核失败
     * 3 人工审核
     * 4 人工审核通过
     * 8 审核通过（待发布）
     * 9 已发布
     */
    private Byte status;
    /**
     * 提交时间
     */
    private Date submitedTime;
    /**
     * 创建时间
     */
    private Date createdTime;

    //状态枚举类
    @Alias("WmNewsStatus")
    public enum Status {
        NORMAL((byte) 0), SUBMIT((byte) 1), FAIL((byte) 2), ADMIN_AUTH((byte) 3), ADMIN_SUCCESS((byte) 4), SUCCESS((byte) 8), PUBLISHED((byte) 9);
        byte code;

        Status(byte code) {
            this.code = code;
        }

        public byte getCode() {
            return this.code;
        }
    }
}