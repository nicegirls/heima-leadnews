package com.heima.model.wemedia.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

import java.util.Date;

/**
 * @author 韦东
 * @date 2021/9/5 周日
 */
@Data
public class WmNewsPageDTO extends PageRequestDto {
    /**
     * 关键字
     */
    private String keyword;
    /**
     * 频道id
     */
    private Integer channelId;
    /**
     * 状态
     */
    private Byte status;
    /**
     * 开始日期
     */
    private Date beginPubDate;
    /**
     * 结束日期
     */
    private Date endPubDate;

}
