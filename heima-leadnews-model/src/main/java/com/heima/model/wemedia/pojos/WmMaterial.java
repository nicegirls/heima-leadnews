package com.heima.model.wemedia.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 自媒体图文素材信息表
 *
 * @TableName wm_material
 */
@TableName(value = "wm_material")
@Data
public class WmMaterial implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 自媒体用户ID
     */
    private Integer userId;
    /**
     * 图片地址
     */
    private String url;
    /**
     * 是否收藏
     */
    private Byte isCollection;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 素材类型
     * 0 图片
     * 1 视频
     */
    private Byte type;

}