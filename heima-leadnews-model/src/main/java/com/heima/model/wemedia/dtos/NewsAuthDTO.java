package com.heima.model.wemedia.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author 韦东
 * @date 2021/9/10 周五
 */

@Data
public class NewsAuthDTO extends PageRequestDto {

    private String title;

    private String msg;
    private Integer id;
}
