package com.heima.model.wemedia.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author 韦东
 * @date 2021/9/4 周六
 */
@Data
public class MaterialDTO extends PageRequestDto {

    /**
     * 是否是收藏
     * 1是
     * 0不是
     */
    private Byte isCollection;
}
