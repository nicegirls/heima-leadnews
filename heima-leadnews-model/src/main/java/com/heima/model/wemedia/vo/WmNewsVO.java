package com.heima.model.wemedia.vo;

import com.heima.model.wemedia.pojos.WmNews;
import lombok.Data;

/**
 * @author 韦东
 * @date 2021/9/10 周五
 */
@Data
public class WmNewsVO extends WmNews {

    private static final long serialVersionUID = -8836145572547625143L;
    private String authorName;
}
