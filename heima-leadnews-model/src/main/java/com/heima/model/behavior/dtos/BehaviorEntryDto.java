package com.heima.model.behavior.dtos;

import lombok.Data;

/**
 * @author 韦东
 * @date 2021/9/12 周日
 */
@Data
public class BehaviorEntryDto {
    private Integer userId;
    private Integer equipmentId;
}
