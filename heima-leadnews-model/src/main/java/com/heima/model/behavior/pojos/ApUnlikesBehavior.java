package com.heima.model.behavior.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * APP不喜欢行为表
 *
 * @TableName ap_unlikes_behavior
 */
@TableName(value = "ap_unlikes_behavior")
@Data
public class ApUnlikesBehavior implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    /**
     * 实体ID
     */
    private Integer entryId;
    /**
     * 文章ID
     */
    private Long articleId;
    /**
     * 0 不喜欢
     * 1 取消不喜欢
     */
    private short type;
    /**
     * 登录时间
     */
    private Date createdTime;
}