package com.heima.app.gateway.filters;

import com.heima.app.gateway.utils.AppJwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 鉴权过滤器
 *
 * @author 韦东
 * @date 2021-09-02
 */
@Component
@Log4j2
public class AuthorizeFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        //获取请求响应对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //如果是登录请求 放行
        if (request.getURI().getPath().contains("login")) {
            return chain.filter(exchange);
        }

        //获取请求头中的token
        String token = request.getHeaders().getFirst("token");

        //没有携带token 返回401
        if (StringUtils.isBlank(token)) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        //判断token是否有效
        Claims claims = AppJwtUtil.getClaimsBody(token);
        int verifyCode = AppJwtUtil.verifyToken(claims);
        //token过期
        if (verifyCode == 0) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        //获取token中id
        assert claims != null;
        Integer id = (Integer) claims.get("id");
        if (id == null) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        //记录日志
        log.info("find userid:{} from uri:{}", id, request.getURI());
        //把id重新设置给header
        ServerHttpRequest httpRequest = request.mutate().headers(httpHeaders -> httpHeaders.add("userId", String.valueOf(id))).build();
        exchange.mutate().request(httpRequest).build();

        //放行
        return chain.filter(exchange);

    }

    @Override
    public int getOrder() {
        return 0;
    }
}
