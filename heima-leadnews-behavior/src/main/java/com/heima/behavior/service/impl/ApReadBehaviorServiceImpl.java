package com.heima.behavior.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.behavior.mapper.ApReadBehaviorMapper;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.behavior.service.ApReadBehaviorService;
import com.heima.model.behavior.dtos.ReadBehaviorDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApReadBehavior;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
@Log4j2
public class ApReadBehaviorServiceImpl extends ServiceImpl<ApReadBehaviorMapper, ApReadBehavior> implements ApReadBehaviorService {

    @Resource
    private ApBehaviorEntryService apBehaviorEntryService;

    @Override
    public ResponseResult readBehavior(ReadBehaviorDto dto) {
        //检查参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询行为实体
        Integer userId = AppThreadLocalUtils.getUser().getId();
        ApBehaviorEntry entry = this.apBehaviorEntryService.findByUserIdOrEquipmentId(userId, dto.getEquipmentId());
        if (entry == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //保存或更新阅读行为
        //查询是否有阅读行为记录
        ApReadBehavior readBehavior = this.getOne(Wrappers.<ApReadBehavior>lambdaQuery().eq(ApReadBehavior::getArticleId, dto.getArticleId()).eq(ApReadBehavior::getEntryId, entry.getId()));
        //没有记录 执行新增逻辑
        if (readBehavior == null) {
            readBehavior = new ApReadBehavior();
            readBehavior.setEntryId(entry.getId());
            readBehavior.setArticleId(dto.getArticleId());
            readBehavior.setCount(dto.getCount());
            readBehavior.setReadDuration(dto.getReadDuration());
            readBehavior.setPercentage(dto.getPercentage());
            readBehavior.setLoadDuration(dto.getLoadDuration());
            readBehavior.setCreatedTime(new Date());

            this.save(readBehavior);
        } else {//有记录 执行更新逻辑
            readBehavior.setCount((short) (readBehavior.getCount() + 1));
            readBehavior.setUpdatedTime(new Date());
            this.updateById(readBehavior);
        }
        return ResponseResult.okResult(null);
    }
}
