package com.heima.behavior.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.behavior.mapper.ApBehaviorEntryMapper;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import org.springframework.stereotype.Service;

@Service
public class ApBehaviorEntryServiceImpl extends ServiceImpl<ApBehaviorEntryMapper, ApBehaviorEntry> implements ApBehaviorEntryService {

    @Override
    public ApBehaviorEntry findByUserIdOrEquipmentId(Integer userId, Integer equipmentId) {
        if (userId != null) {
            LambdaQueryWrapper<ApBehaviorEntry> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(ApBehaviorEntry::getEntryId, userId).eq(ApBehaviorEntry::getType, 1);
            return this.getOne(queryWrapper);
        }

        if (equipmentId != null) {
            LambdaQueryWrapper<ApBehaviorEntry> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(ApBehaviorEntry::getEntryId, equipmentId).eq(ApBehaviorEntry::getType, 0);
            return this.getOne(queryWrapper);
        }
        return null;
    }
}