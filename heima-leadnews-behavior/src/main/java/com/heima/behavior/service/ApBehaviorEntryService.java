package com.heima.behavior.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.behavior.pojos.ApBehaviorEntry;

public interface ApBehaviorEntryService extends IService<ApBehaviorEntry> {
    ApBehaviorEntry findByUserIdOrEquipmentId(Integer userId, Integer equipmentId);
}