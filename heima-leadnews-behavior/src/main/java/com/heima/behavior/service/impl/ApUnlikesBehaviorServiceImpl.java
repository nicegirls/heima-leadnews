package com.heima.behavior.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.behavior.mapper.ApUnlikesBehaviorMapper;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.behavior.service.ApUnlikesBehaviorService;
import com.heima.model.behavior.dtos.UnLikesBehaviorDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 *
 */
@Service
public class ApUnlikesBehaviorServiceImpl extends ServiceImpl<ApUnlikesBehaviorMapper, ApUnlikesBehavior>
        implements ApUnlikesBehaviorService {

    @Resource
    private ApBehaviorEntryService apBehaviorEntryService;

    @Override
    public ResponseResult unlike(UnLikesBehaviorDto dto) {
        //检查参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询行为实体
        ApUser apUser = AppThreadLocalUtils.getUser();
        Integer userId = apUser.getId();
        ApBehaviorEntry entry = this.apBehaviorEntryService.findByUserIdOrEquipmentId(userId, dto.getEquipmentId());
        if (entry == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询不喜欢记录
        ApUnlikesBehavior unlikesBehavior = this.getOne(Wrappers.<ApUnlikesBehavior>lambdaQuery().eq(ApUnlikesBehavior::getArticleId, dto.getArticleId()).eq(ApUnlikesBehavior::getEntryId, entry.getId()));
        //记录不存在 并且 操作为不喜欢 执行新增逻辑
        if (unlikesBehavior == null && dto.getType() == 0) {
            unlikesBehavior = new ApUnlikesBehavior();
            unlikesBehavior.setEntryId(entry.getId());
            unlikesBehavior.setArticleId(dto.getArticleId());
            unlikesBehavior.setType(dto.getType());
            unlikesBehavior.setCreatedTime(new Date());
            this.save(unlikesBehavior);
        } else {//记录存在 执行更新逻辑
            unlikesBehavior.setType(dto.getType());
            this.updateById(unlikesBehavior);
        }
        return ResponseResult.okResult(null);
    }
}




