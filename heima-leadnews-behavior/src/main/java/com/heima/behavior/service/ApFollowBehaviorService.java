package com.heima.behavior.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.behavior.dtos.FollowBehaviorDto;
import com.heima.model.behavior.pojos.ApFollowBehavior;
import com.heima.model.common.dtos.ResponseResult;

public interface ApFollowBehaviorService extends IService<ApFollowBehavior> {
    ResponseResult saveFollowBehavior(FollowBehaviorDto dto);
}