package com.heima.behavior.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.behavior.mapper.ApLikesBehaviorMapper;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.behavior.service.ApLikesBehaviorService;
import com.heima.model.behavior.dtos.LikesBehaviorDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApLikesBehavior;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class ApLikesBehaviorServiceImpl extends ServiceImpl<ApLikesBehaviorMapper, ApLikesBehavior> implements ApLikesBehaviorService {

    @Resource
    private ApBehaviorEntryService apBehaviorEntryService;

    @Override
    public ResponseResult like(LikesBehaviorDto dto) {
        //检查参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询行为实体
        ApUser apUser = AppThreadLocalUtils.getUser();
        Integer userId = apUser.getId();
        ApBehaviorEntry entry = this.apBehaviorEntryService.findByUserIdOrEquipmentId(userId, dto.getEquipmentId());
        if (entry == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //点赞或取消点赞
        //查询点赞行为
        ApLikesBehavior likesBehavior = this.getOne(Wrappers.<ApLikesBehavior>lambdaQuery().eq(ApLikesBehavior::getArticleId, dto.getArticleId()).eq(ApLikesBehavior::getEntryId, entry.getId()));
        //数据库没有记录 并且操作是点赞 执行新增
        if (likesBehavior == null && dto.getOperation() == 0) {
            likesBehavior = new ApLikesBehavior();
            likesBehavior.setEntryId(entry.getId());
            likesBehavior.setArticleId(dto.getArticleId());
            likesBehavior.setType(dto.getType());
            likesBehavior.setOperation(dto.getOperation());
            likesBehavior.setCreatedTime(new Date());
            this.save(likesBehavior);
        } else {
            likesBehavior.setOperation(dto.getOperation());
            this.updateById(likesBehavior);
        }
        return ResponseResult.okResult(null);
    }
}