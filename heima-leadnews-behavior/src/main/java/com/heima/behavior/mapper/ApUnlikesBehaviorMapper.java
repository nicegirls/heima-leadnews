package com.heima.behavior.mapper;

import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.heima.model.behavior.pojos.ApUnlikesBehavior
 */
public interface ApUnlikesBehaviorMapper extends BaseMapper<ApUnlikesBehavior> {

}




