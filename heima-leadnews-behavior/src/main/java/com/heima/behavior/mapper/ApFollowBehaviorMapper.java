package com.heima.behavior.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.behavior.pojos.ApFollowBehavior;

public interface ApFollowBehaviorMapper extends BaseMapper<ApFollowBehavior> {

}