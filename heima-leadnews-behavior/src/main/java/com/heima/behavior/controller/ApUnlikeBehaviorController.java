package com.heima.behavior.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.behavior.service.ApUnlikesBehaviorService;
import com.heima.model.behavior.dtos.UnLikesBehaviorDto;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/12 周日
 */
@RestController
@RequestMapping("api/v1/unlike_behavior")
public class ApUnlikeBehaviorController {

    @Resource
    private ApUnlikesBehaviorService apUnlikesBehaviorService;

    @PostMapping
    public ResponseResult unlike(@RequestBody UnLikesBehaviorDto dto) {
        return this.apUnlikesBehaviorService.unlike(dto);
    }

    @GetMapping("/one")
    public ApUnlikesBehavior findUnLikeByArticleIdAndEntryId(@RequestParam("articleId") Long articleId, @RequestParam("entryId") Integer entryId) {
        return this.apUnlikesBehaviorService.getOne(Wrappers.<ApUnlikesBehavior>lambdaQuery()
                .eq(ApUnlikesBehavior::getArticleId, articleId)
                .eq(ApUnlikesBehavior::getEntryId, entryId));
    }
}
