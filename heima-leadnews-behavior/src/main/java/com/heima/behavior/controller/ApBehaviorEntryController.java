package com.heima.behavior.controller;

import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.model.behavior.dtos.BehaviorEntryDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/12 周日
 */
@RestController
@RequestMapping("api/v1/behavior_entry")
public class ApBehaviorEntryController {

    @Resource
    private ApBehaviorEntryService apBehaviorEntryService;

    @PostMapping
    public ApBehaviorEntry findByUserIdOrEquipmentId(@RequestBody BehaviorEntryDto dto) {
        if (dto != null) {
            return this.apBehaviorEntryService.findByUserIdOrEquipmentId(dto.getUserId(), dto.getEquipmentId());
        }
        return null;
    }
}
