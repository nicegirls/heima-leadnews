package com.heima.common.constants;

/**
 * @author 韦东
 * @date 2021/9/2 周四
 */
public class UserConstans {
    /**
     * 审核通过
     */
    public static final Byte PASS_AUTH = 9;
    /**
     * 审核不通过
     */
    public static final Byte FAIL_AUTH = 2;
    /**
     * 作者类型 自媒体人
     */
    public static final Byte AUTH_TYPE = 2;
}
