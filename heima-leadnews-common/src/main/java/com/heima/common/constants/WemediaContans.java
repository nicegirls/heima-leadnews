package com.heima.common.constants;

/**
 * wemedia contans
 *
 * @author 韦东
 * @date 2021-09-05
 */
public class WemediaContans {

    public static final Byte COLLECT_MATERIAL = 1;//收藏

    public static final Byte CANCEL_COLLECT_MATERIAL = 0;//取消收藏

    /**
     * 类型为图片
     */
    public static final String WM_NEWS_TYPE_IMAGE = "image";

    /**
     * 无图
     */
    public static final Byte WM_NEWS_NONE_IMAGE = 0;
    /**
     * 单图
     */
    public static final Byte WM_NEWS_ONE_IMAGE = 1;
    /**
     * 多图
     */
    public static final Byte WM_NEWS_MANY_IMAGE = 3;
    /**
     * 自动
     */
    public static final Byte WM_NEWS_TYPE_AUTO = -1;

    /**
     * 正文中的图片
     */
    public static final Byte WM_CONTENT_REFERENCE = 0;
    /**
     * 标题中的图片
     */
    public static final Byte WM_COVER_REFERENCE = 1;

    /**
     * 上架中
     */
    public static final Byte WM_NEWS_ENABLE_UP = 1;
}
