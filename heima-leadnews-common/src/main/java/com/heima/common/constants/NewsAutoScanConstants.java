package com.heima.common.constants;

/**
 * 自媒体文章自动审核常量
 *
 * @author 韦东
 * @date 2021-09-08
 */
public class NewsAutoScanConstants {

    public static final String WM_NEWS_AUTO_SCAN_TOPIC = "wm.news.auto.scan.topic";
}