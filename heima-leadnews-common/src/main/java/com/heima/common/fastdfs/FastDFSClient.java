package com.heima.common.fastdfs;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.domain.proto.storage.DownloadCallback;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * fastDFS客户端工具
 *
 * @author 韦东
 * @date 2021-09-03
 */
@Component
public class FastDFSClient {

    @Autowired
    private FastFileStorageClient storageClient;

    /**
     * 上传文件
     *
     * @param file 文件
     * @return {@link String}
     * @throws IOException ioexception
     */
    public String uploadFile(MultipartFile file) throws IOException {
        StorePath storePath = this.storageClient.uploadFile((InputStream) file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
        return storePath.getFullPath();
    }

    /**
     * 删除文件
     *
     * @param filePath 文件路径
     */
    public void delFile(String filePath) {
        this.storageClient.deleteFile(filePath);

    }

    /**
     * 下载文件
     *
     * @return {@link byte[]}
     * @throws IOException ioexception
     */
    public byte[] download(String groupName, String path) throws IOException {
        InputStream ins = this.storageClient.downloadFile(groupName, path, new DownloadCallback<InputStream>() {
            @Override
            public InputStream recv(InputStream ins) throws IOException {
                // 将此ins返回给上面的ins
                return ins;
            }
        });

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buff = new byte[100];
        int rc = 0;
        while ((rc = ins.read(buff, 0, 100)) > 0) byteArrayOutputStream.write(buff, 0, rc);
        return byteArrayOutputStream.toByteArray();
    }
}