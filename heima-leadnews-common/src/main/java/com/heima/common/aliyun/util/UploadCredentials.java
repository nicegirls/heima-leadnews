package com.heima.common.aliyun.util;

import java.io.Serializable;

public class UploadCredentials implements Serializable {

    private static final long serialVersionUID = -4350429642490855095L;
    private String accessKeyId;
    private String accessKeySecret;
    private String securityToken;
    private Long expiredTime;
    private String ossEndpoint;
    private String ossInternalEndpoint;
    private String uploadBucket;
    private String uploadFolder;

    public UploadCredentials(String accessKeyId, String accessKeySecret, String securityToken, Long expiredTime, String ossEndpoint, String ossInternalEndpoint, String uploadBucket, String uploadFolder) {
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
        this.securityToken = securityToken;
        this.expiredTime = expiredTime;
        this.ossEndpoint = ossEndpoint;
        this.ossInternalEndpoint = ossInternalEndpoint;
        this.uploadBucket = uploadBucket;
        this.uploadFolder = uploadFolder;
    }

    public String getAccessKeyId() {
        return this.accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return this.accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getSecurityToken() {
        return this.securityToken;
    }

    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }

    public Long getExpiredTime() {
        return this.expiredTime;
    }

    public void setExpiredTime(Long expiredTime) {
        this.expiredTime = expiredTime;
    }

    public String getOssEndpoint() {
        return this.ossEndpoint;
    }

    public void setOssEndpoint(String ossEndpoint) {
        this.ossEndpoint = ossEndpoint;
    }

    public String getUploadBucket() {
        return this.uploadBucket;
    }

    public void setUploadBucket(String uploadBucket) {
        this.uploadBucket = uploadBucket;
    }

    public String getUploadFolder() {
        return this.uploadFolder;
    }

    public void setUploadFolder(String uploadFolder) {
        this.uploadFolder = uploadFolder;
    }

    public String getOssInternalEndpoint() {
        return this.ossInternalEndpoint;
    }

    public void setOssInternalEndpoint(String ossInternalEndpoint) {
        this.ossInternalEndpoint = ossInternalEndpoint;
    }
}
