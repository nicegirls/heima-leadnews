package com.heima.common.exception;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 韦东
 * @date 2021/9/1 周三
 */
@ControllerAdvice
@ResponseBody
@Log4j2
public class ExceptionCatch {

    @ExceptionHandler(Exception.class)
    public ResponseResult exceptionCatch(Exception e){
        //打印错误
        e.printStackTrace();
        //记录日志
        log.error("catch exception:{}",e.getMessage());

        //返回通用异常
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
    }
}
