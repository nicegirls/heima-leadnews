package com.heima.admin.feign;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDTO;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.model.wemedia.vo.WmNewsVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 韦东
 * @date 2021/9/8 周三
 */
@FeignClient("leadnews-wemedia")
@RequestMapping("api/v1")
public interface WemediaFeign {

    /**
     * 更新文章审核状态
     *
     * @param wmNews 文章
     * @return {@link ResponseResult}
     */
    @PostMapping("news/update")
    public ResponseResult updateWmNews(@RequestBody WmNews wmNews);

    /**
     * 通过id 查询
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    @GetMapping("news/findOne/{id}")
    public WmNews getWmNewsById(@PathVariable Integer id);

    /**
     * 通过id 查询自媒体用户
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    @GetMapping("user/findById/{id}")
    public WmUser findWmUserById(@PathVariable Integer id);

    /**
     * 查找所有 待发布中已到发布时间的 自媒体文章id
     *
     * @return {@link List}<{@link Integer}>
     */
    @GetMapping("news/findReleaseable")
    public List<Integer> findReleasedable();

    /**
     * 根据标题模糊分页查询自媒体文章列表
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    @PostMapping("news/findListWithTitleAndPage")
    public ResponseResult findListWithTitleAndPage(@RequestBody NewsAuthDTO dto);

    /**
     * 查询自媒体文章详情
     *
     * @param id id
     * @return {@link WmNewsVO}
     */
    @GetMapping("news/find_news_vo/{id}")
    public WmNewsVO FindNewsVO(@PathVariable Integer id);
}
