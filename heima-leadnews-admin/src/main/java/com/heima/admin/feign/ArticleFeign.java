package com.heima.admin.feign;

import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.article.pojos.ApArticleContent;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author 韦东
 * @date 2021/9/8 周三
 */
@FeignClient("leadnews-article")
@RequestMapping("api/v1")
public interface ArticleFeign {

    /**
     * 根据名字查询作者
     *
     * @param name 名字
     * @return {@link ResponseResult}
     */
    @GetMapping("author/findByName/{name}")
    public ApAuthor findByName(@PathVariable String name);

    /**
     * 保存文章
     *
     * @param article 文章
     * @return {@link ResponseResult}
     */
    @PostMapping("article/save")
    public ApArticle saveArticle(@RequestBody ApArticle article);

    /**
     * 保存文章内容
     *
     * @param apArticleContent 文章内容
     * @return {@link ResponseResult}
     */
    @PostMapping("article_content/save")
    public ResponseResult saveArticleContent(@RequestBody ApArticleContent apArticleContent);

    /**
     * 保存文章配置
     *
     * @param articleConfig 配置
     * @return {@link ResponseResult}
     */
    @PostMapping("article_config/save")
    public ResponseResult saveArticleConfig(@RequestBody ApArticleConfig articleConfig);
}
