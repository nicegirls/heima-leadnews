package com.heima.admin.controller.v1;

import com.heima.admin.service.AdSensitiveService;
import com.heima.api.admin.AdSensitiveControllerApi;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/1 周三
 */
@RequestMapping("api/v1/sensitive")
@RestController
public class AdSensitiveController implements AdSensitiveControllerApi {

    @Resource
    private AdSensitiveService adSensitiveService;

    @PostMapping("list")
    @Override
    public ResponseResult findByNameAndPage(SensitiveDto dto) {
        return this.adSensitiveService.findByNameAndPage(dto);
    }

    @PostMapping("save")
    @Override
    public ResponseResult insert(@RequestBody AdSensitive adSensitive) {
        return this.adSensitiveService.insertAdSensitive(adSensitive);
    }

    @DeleteMapping("del/{id}")
    @Override
    public ResponseResult delete(@PathVariable Integer id) {
        return this.adSensitiveService.deleteAdSensitive(id);
    }

    @PostMapping("update")
    @Override
    public ResponseResult update(@RequestBody AdSensitive adSensitive) {
        return this.adSensitiveService.updateAdSensitive(adSensitive);
    }
}
