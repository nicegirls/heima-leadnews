package com.heima.admin.controller.v1;

import com.heima.admin.service.WemediaNewsAutoScanService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDTO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/10 周五
 */
@RestController
@RequestMapping("api/v1/news_auth")
public class NewsAuthController {

    @Resource
    private WemediaNewsAutoScanService wemediaNewsAutoScanService;

    @PostMapping("list")
    public ResponseResult findList(@RequestBody NewsAuthDTO dto) {
        return this.wemediaNewsAutoScanService.findNews(dto);
    }

    @GetMapping("one/{id}")
    public ResponseResult findById(@PathVariable Integer id) {
        return this.wemediaNewsAutoScanService.findOne(id);
    }

    @PostMapping("auth_pass")
    public ResponseResult authPass(@RequestBody NewsAuthDTO dto) {
        return this.wemediaNewsAutoScanService.updateStatus(1, dto);
    }

    @PostMapping("auth_fail")
    public ResponseResult authFail(@RequestBody NewsAuthDTO dto) {
        return this.wemediaNewsAutoScanService.updateStatus(0, dto);
    }
}
