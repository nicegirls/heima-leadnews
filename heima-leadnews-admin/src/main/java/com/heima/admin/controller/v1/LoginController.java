package com.heima.admin.controller.v1;

import com.heima.admin.service.AdUserService;
import com.heima.api.admin.LoginControllerApi;
import com.heima.model.admin.pojos.AdUser;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/1 周三
 */
@RestController
@RequestMapping("login")
public class LoginController implements LoginControllerApi {

    @Resource
    private AdUserService adUserService;

    @Override
    @PostMapping("in")
    public ResponseResult login(AdUser adUser) {
        return this.adUserService.login(adUser);
    }
}
