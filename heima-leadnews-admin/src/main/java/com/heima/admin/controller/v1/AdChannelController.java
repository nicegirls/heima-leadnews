package com.heima.admin.controller.v1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.heima.admin.service.AdChannelService;
import com.heima.api.admin.AdChannelControllerApi;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 韦东
 * @date 2021/8/30 周一
 */
@RestController
@RequestMapping("api/v1/channel")
public class AdChannelController implements AdChannelControllerApi {

    @Resource
    private AdChannelService channelService;

    @Override
    @PostMapping("list")
    public ResponseResult findByNameAndPage(ChannelDto channelDto) {
        return this.channelService.findByNameAndPage(channelDto);
    }


    @PostMapping("save")
    @Override
    public ResponseResult insertChannel(@RequestBody AdChannel adChannel) {
        return this.channelService.insertChannel(adChannel);
    }

    @GetMapping("/del/{id}")
    @Override
    public ResponseResult deleteChannel(@PathVariable Integer id) {
        return this.channelService.deleteChannel(id);
    }

    @PostMapping("update")
    @Override
    public ResponseResult updateChannel(@RequestBody AdChannel adChannel) {
        return this.channelService.updateChannel(adChannel);
    }

    @GetMapping("channels")
    public ResponseResult findAll() {
        List<AdChannel> list = this.channelService.list(new LambdaQueryWrapper<AdChannel>().eq(AdChannel::getStatus, 1));
        return ResponseResult.okResult(list);
    }
}
