package com.heima.admin.job;

import com.heima.admin.feign.WemediaFeign;
import com.heima.admin.service.WemediaNewsAutoScanService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 韦东
 * @date 2021/9/10 周五
 */
@Component
@Log4j2
public class WeMediaNewsAutoScanJob {
    @Resource
    private WemediaNewsAutoScanService wemediaNewsAutoScanService;

    @Resource
    private WemediaFeign wemediaFeign;

    /**
     * 扫描所有到达发布时间的自媒体文章 发布
     *
     * @return {@link ReturnT}<{@link String}>
     */
    @XxlJob("scanWeMediaNews")
    public ReturnT<String> scanWeMediaNews(String param) {
        log.info("自媒体文章审核调度任务开始执行....");
        List<Integer> list = this.wemediaFeign.findReleasedable();
        if (!CollectionUtils.isEmpty(list)) {
            for (Integer id : list) {
                this.wemediaNewsAutoScanService.autoScanByWemediaId(id);
            }
        }

        log.info("自媒体文章审核调度任务执行结束....");
        return ReturnT.SUCCESS;
    }
}
