package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;

/**
 * 频道服务
 *
 * @author 韦东
 * @date 2021-08-30
 */
public interface AdChannelService extends IService<AdChannel> {

    /**
     * 根据名称分页查询频道列表
     *
     * @param channelDto 频道dto
     * @return {@link ResponseResult}
     */
    ResponseResult findByNameAndPage(ChannelDto channelDto);

    /**
     * 新增频道
     *
     * @param adChannel 频道
     * @return {@link ResponseResult}
     */
    ResponseResult insertChannel(AdChannel adChannel);

    /**
     * 删除频道
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    ResponseResult deleteChannel(Integer id);

    /**
     * 更新频道
     *
     * @param adChannel 频道
     * @return {@link ResponseResult}
     */
    ResponseResult updateChannel(AdChannel adChannel);
}
