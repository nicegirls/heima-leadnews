package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.pojos.AdUser;
import com.heima.model.common.dtos.ResponseResult;

/**
 *
 */
public interface AdUserService extends IService<AdUser> {

    /**
     * 登录
     *
     * @param adUser (用户名+密码)
     * @return {@link ResponseResult}
     */
    ResponseResult login(AdUser adUser);
}
