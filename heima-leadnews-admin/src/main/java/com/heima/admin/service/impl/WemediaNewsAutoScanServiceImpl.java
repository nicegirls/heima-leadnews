package com.heima.admin.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.admin.feign.ArticleFeign;
import com.heima.admin.feign.WemediaFeign;
import com.heima.admin.service.AdChannelService;
import com.heima.admin.service.AdSensitiveService;
import com.heima.admin.service.WemediaNewsAutoScanService;
import com.heima.common.aliyun.GreenImageScan;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.common.fastdfs.FastDFSClient;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.article.pojos.ApArticleContent;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.NewsAuthDTO;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.model.wemedia.vo.WmNewsVO;
import com.heima.utils.common.SensitiveWordUtil;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author 韦东
 * @date 2021/9/8 周三
 */
@Service
@Log4j2
public class WemediaNewsAutoScanServiceImpl implements WemediaNewsAutoScanService {

    @Resource
    private WemediaFeign wemediaFeign;

    @Resource
    private ArticleFeign articleFeign;

    @Resource
    private AdChannelService channelService;

    @Value("${fdfs.url}")
    private String fileServerUrl;

    @Resource
    private GreenImageScan greenImageScan;

    @Resource
    private GreenTextScan greenTextScan;

    @Resource
    private AdSensitiveService adSensitiveService;

    @Resource
    private FastDFSClient fastDFSClient;

    /**
     * 自动审核自媒体文章
     *
     * @param weMediaId 自媒体文章id
     */
    @Override
    @GlobalTransactional
    public void autoScanByWemediaId(Integer weMediaId) {
        //检查参数
        if (weMediaId == null) {
            log.error("id不能为空");
            return;
        }
        //根据id查询自媒体文章信息
        WmNews wmNews = this.wemediaFeign.getWmNewsById(weMediaId);
        if (wmNews == null) {
            log.error("自媒体文章不存在, 文章id:{}" + weMediaId);
            return;
        }

        //状态为4 人工审核通过 直接保存app文章/配置/内容
        if (wmNews.getStatus() == 4) {
            this.saveApArticleAndConfigAndContent(wmNews);
            return;
        }
        //状态为8  发布时间小于等于当前时间 直接保存app文章/配置/内容
        if (wmNews.getStatus() == 8 && wmNews.getPublishTime().before(new Date())) {
            this.saveApArticleAndConfigAndContent(wmNews);
            return;
        }

        //状态为1 审核
        if (wmNews.getStatus() == 1) {
            //解析文章内容中的纯文本和图片 和 封面中的图片
            Map<String, Object> imagesAndTexts = this.handleImagesAndText(wmNews);
            List<String> images = (List<String>) imagesAndTexts.get("images");
            String text = (String) imagesAndTexts.get("text");

            //审核图片
            Boolean isSuccess = this.auditImages(images, wmNews);
            //审核失败直接返回
            if (!isSuccess) {
                return;
            }
            //审核文本
            isSuccess = this.auditText(text, wmNews);
            //审核失败直接返回
            if (!isSuccess) {
                return;
            }
            //自管理的敏感词审核
            isSuccess = this.auditSensitive(text, wmNews);
            //审核失败直接返回
            if (!isSuccess) {
                return;
            }

            //发布时间大于当前时间 修改自媒体文章状态为8 审核通过待发布
            if (wmNews.getPublishTime().after(new Date())) {
                this.updateWmNews(wmNews, (byte) 8, "审核通过, 待发布");
                return;
            }

            //审核通过 保存app文章/配置/内容 修改自媒体文章状态为9
            this.saveApArticleAndConfigAndContent(wmNews);
        }

    }

    @Override
    public ResponseResult findNews(NewsAuthDTO dto) {
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        ResponseResult result = this.wemediaFeign.findListWithTitleAndPage(dto);
        result.setHost(this.fileServerUrl);
        return result;
    }

    @Override
    public ResponseResult findOne(Integer id) {
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        WmNewsVO wmNewsVO = this.wemediaFeign.FindNewsVO(id);
        ResponseResult result = ResponseResult.okResult(wmNewsVO);
        result.setHost(this.fileServerUrl);
        return result;
    }

    @Override
    public ResponseResult updateStatus(int type, NewsAuthDTO dto) {
        //1.参数检查
        if (dto == null || dto.getId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        WmNews wmNews = this.wemediaFeign.getWmNewsById(dto.getId());
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        if (type == 1) {
            this.updateWmNews(wmNews, (byte) 4, "人工审核通过");
        } else if (type == 0) {
            this.updateWmNews(wmNews, (byte) 2, dto.getMsg());
        }

        return ResponseResult.okResult(null);
    }

    /**
     * 保存app文章/配置/内容 修改自媒体文章状态为9
     *
     * @param wmNews 自媒体文章
     */
    private void saveApArticleAndConfigAndContent(WmNews wmNews) {
        //保存app文章
        ApArticle apArticle = this.saveApArticle(wmNews);
        //保存文章配置
        this.saveApArticleConfig(apArticle.getId());
        //保存文章内容
        this.saveApArticleContent(wmNews.getContent(), apArticle.getId());
        //修改自媒体文章状态和对应app文章id
        wmNews.setArticleId(apArticle.getId());
        this.updateWmNews(wmNews, (byte) 9, "审核通过");
    }

    /**
     * 保存app文章内容
     *
     * @param content   自媒体文章内容
     * @param articleId app文章id
     */
    private void saveApArticleContent(String content, Long articleId) {
        ApArticleContent apArticleContent = new ApArticleContent();
        apArticleContent.setContent(content);
        apArticleContent.setArticleId(articleId);
        try {
            this.articleFeign.saveArticleContent(apArticleContent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存app文章配置
     *
     * @param apArticleId app文章id
     */
    private void saveApArticleConfig(Long apArticleId) {
        ApArticleConfig apArticleConfig = new ApArticleConfig();
        apArticleConfig.setArticleId(apArticleId);
        apArticleConfig.setIsComment(true);
        apArticleConfig.setIsForward(true);
        apArticleConfig.setIsDown(false);
        apArticleConfig.setIsDelete(false);

        try {
            this.articleFeign.saveArticleConfig(apArticleConfig);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 审核自管理的敏感词
     *
     * @param text   文本
     * @param wmNews 自媒体文章
     * @return {@link Boolean}
     */
    private Boolean auditSensitive(String text, WmNews wmNews) {
        List<String> adSensitiveList = this.adSensitiveService.findAll();
        SensitiveWordUtil.initMap(adSensitiveList);
        Map<String, Integer> map = SensitiveWordUtil.matchWords(text);
        if (map.size() > 0) {
            log.error("敏感词过滤没有通过,敏感词:{}" + map);
            this.updateWmNews(wmNews, (byte) 2, "文本中有敏感词");
            return false;
        }
        return true;
    }

    /**
     * 审核文本
     *
     * @param text   文本
     * @param wmNews wm新闻
     * @return {@link Boolean}
     */
    private Boolean auditText(String text, WmNews wmNews) {
        try {
            Map map = this.greenTextScan.greeTextScan(text);
            if (!map.get("suggestion").equals("pass")) {
                //违规
                if (map.get("suggestion").equals("block")) {
                    //修改自媒体文章状态和失败原因
                    this.updateWmNews(wmNews, (byte) 2, "文本中有违规内容");
                    return false;
                }
                if (map.get("suggestion").equals("review")) {
                    this.updateWmNews(wmNews, (byte) 3, "文本中有不确定因素");
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 审核图像
     *
     * @param images 图片
     * @param wmNews
     * @return {@link Boolean}
     */
    private Boolean auditImages(List<String> images, WmNews wmNews) {
        //如果没有图片 直接返回
        if (CollectionUtils.isEmpty(images)) {
            return true;
        }

        List<byte[]> imageList = new ArrayList<>();
        //调用阿里云图片审核
        try {

            for (String image : images) {
                String imageName = image.replace(this.fileServerUrl, "");
                int index = imageName.indexOf("/");
                String groupName = imageName.substring(0, index);
                String imagePath = imageName.substring(index + 1);
                byte[] imageByte = this.fastDFSClient.download(groupName, imagePath);
                imageList.add(imageByte);
            }

            Map map = this.greenImageScan.imageScan(imageList);
            //审核不通过
            if (!map.get("suggestion").equals("pass")) {
                //违规
                if (map.get("suggestion").equals("block")) {
                    //修改自媒体文章状态和失败原因
                    this.updateWmNews(wmNews, (byte) 2, "图片中有违规内容");
                    return false;
                }
                if (map.get("suggestion").equals("review")) {
                    this.updateWmNews(wmNews, (byte) 3, "图片中有不确定因素");
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 修改自媒体文章状态和审核失败原因
     *
     * @param wmNews 自媒体文章
     * @param status 状态
     * @param reason 原因
     */
    private void updateWmNews(WmNews wmNews, byte status, String reason) {
        wmNews.setStatus(status);
        wmNews.setReason(reason);
        //远程调用修改
        this.wemediaFeign.updateWmNews(wmNews);
    }

    /**
     * 解析文章内容中的纯文本和图片 和 封面中的图片
     *
     * @param wmNews 自媒体文章
     * @return {@link Map}<{@link String}, {@link Object}>
     */
    private Map<String, Object> handleImagesAndText(WmNews wmNews) {
        Map<String, Object> map = new HashMap<>();

        //存放所有图片
        List<String> images = new ArrayList<>();
        //拼接所有文本
        StringBuilder stringBuilder = new StringBuilder();

        //解析内容中的文本和图片
        String content = wmNews.getContent();
        List<Map> lists = JSON.parseArray(content, Map.class);
        for (Map contentMap : lists) {
            if ("image".equals(contentMap.get("type"))) {
                images.add(contentMap.get("value").toString().replace(this.fileServerUrl, ""));
            }
            if ("text".equals(contentMap.get("type"))) {
                stringBuilder.append(contentMap.get("value"));
            }
        }

        //解析封面中的图片
        if (wmNews.getType() != 0 && wmNews.getImages() != null) {
            images.addAll(Arrays.asList(wmNews.getImages().split(",")));
        }

        map.put("images", images);
        map.put("text", stringBuilder.toString());

        return map;
    }

    /**
     * 保存app文章
     *
     * @param wmNews 自媒体文章
     */
    private ApArticle saveApArticle(WmNews wmNews) {
        //准备数据
        ApArticle article = new ApArticle();
        article.setCollection(0);
        article.setImages(wmNews.getImages());
        article.setPublishTime(new Date());
        article.setViews(0);
        article.setComment(0);
        article.setLikes(0);
        article.setLayout(wmNews.getType());
        article.setCreatedTime(new Date());
        article.setLabels(wmNews.getLabels());
        article.setFlag((byte) 0);
        article.setTitle(wmNews.getTitle());

        //查询频道信息
        if (wmNews.getChannelId() != null) {
            AdChannel adChannel = this.channelService.getById(wmNews.getChannelId());
            if (adChannel != null) {
                article.setChannelId(wmNews.getChannelId());
                article.setChannelName(adChannel.getName());
            }
        }

        //查询作者信息
        //先查询自媒体用户信息
        WmUser wmUser = this.wemediaFeign.findWmUserById(wmNews.getUserId());
        if (wmUser == null) {
            log.error("自媒体用户不存在, 自媒体用户id:{}" + wmNews.getUserId());
            return null;
        }
        //根据自媒体用户名字查询作者信息
        ApAuthor apAuthor = this.articleFeign.findByName(wmUser.getName());

        if (apAuthor == null) {
            log.error("作者不存在, 作者名字:{}" + wmUser.getName());
            return null;
        }
        article.setAuthorId(apAuthor.getId());
        article.setAuthorName(apAuthor.getName());
        ApArticle apArticle = this.articleFeign.saveArticle(article);
        return apArticle;
    }
}
