package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;

import java.util.List;

/**
 * 敏感词接口
 *
 * @author 韦东
 * @date 2021-09-01
 */
public interface AdSensitiveService extends IService<AdSensitive> {

    /**
     * 根据名字分页查询
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    ResponseResult findByNameAndPage(SensitiveDto dto);

    /**
     * 新增敏感词
     *
     * @param adSensitive 广告敏感
     * @return {@link ResponseResult}
     */
    ResponseResult insertAdSensitive(AdSensitive adSensitive);

    /**
     * 删除敏感词
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    ResponseResult deleteAdSensitive(Integer id);

    /**
     * 更新敏感词
     *
     * @param adSensitive 广告敏感
     * @return {@link ResponseResult}
     */
    ResponseResult updateAdSensitive(AdSensitive adSensitive);

    /**
     * 查询所有
     *
     * @return {@link List}<{@link String}>
     */
    List<String> findAll();
}
