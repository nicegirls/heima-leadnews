package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdSensitiveMapper;
import com.heima.admin.service.AdSensitiveService;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.function.Function;

/**
 * 敏感词impl
 *
 * @author 韦东
 * @date 2021-09-01
 */
@Service
public class AdSensitiveServiceImpl extends ServiceImpl<AdSensitiveMapper, AdSensitive>
    implements AdSensitiveService{

    @Override
    public ResponseResult findByNameAndPage(SensitiveDto dto) {
        //检查参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        dto.checkParam();

        //模糊查询
        LambdaQueryWrapper<AdSensitive> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(dto.getName())) {
            queryWrapper.like(AdSensitive::getSensitives, dto.getName());
        }

        //分页查询
        IPage<AdSensitive> page = new Page<>(dto.getPage(), dto.getSize());
        this.page(page, queryWrapper);

        //组装数据返回
        return new PageResponseResult(dto.getPage(), dto.getSize(), (int) page.getTotal()).ok(page.getRecords());
    }

    @Override
    public ResponseResult insertAdSensitive(AdSensitive adSensitive) {
        //检查参数
        if (adSensitive == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //设置创建时间
        adSensitive.setCreatedTime(new Date());

        //插入数据
        this.save(adSensitive);

        return ResponseResult.okResult(null);
    }

    @Override
    public ResponseResult deleteAdSensitive(Integer id) {
        //检查参数
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询数据是否存在
        if (this.getById(id) == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //删除数据
        this.removeById(id);

        return ResponseResult.okResult(null);
    }

    @Override
    public ResponseResult updateAdSensitive(AdSensitive adSensitive) {
        //检查参数
        if (adSensitive == null || adSensitive.getId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询数据是否存在
        if (this.getById(adSensitive.getId()) == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //更新数据
        this.updateById(adSensitive);
        return ResponseResult.okResult(null);
    }

    @Override
    public List<String> findAll() {
        LambdaQueryWrapper<AdSensitive> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(AdSensitive::getSensitives);
        List<String> stringList = this.listObjs(queryWrapper, new Function<Object, String>() {
            @Override
            public String apply(Object o) {
                return o.toString();
            }
        });

        return stringList;
    }
}




