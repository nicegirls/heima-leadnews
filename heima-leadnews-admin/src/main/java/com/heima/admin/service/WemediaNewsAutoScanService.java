package com.heima.admin.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDTO;

/**
 * 文章自动审核服务
 *
 * @author 韦东
 * @date 2021-09-08
 */
public interface WemediaNewsAutoScanService {

    public void autoScanByWemediaId(Integer wemediaId);

    ResponseResult findNews(NewsAuthDTO dto);

    ResponseResult findOne(Integer id);

    ResponseResult updateStatus(int type, NewsAuthDTO dto);
}
