package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdChannelMapper;
import com.heima.admin.service.AdChannelService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 频道服务impl
 *
 * @author 韦东
 * @date 2021-08-30
 */
@Service
public class AdChannelServiceImpl extends ServiceImpl<AdChannelMapper, AdChannel>
        implements AdChannelService {

    @Override
    public ResponseResult findByNameAndPage(ChannelDto channelDto) {

        //入参检查
        if (channelDto == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);

        //校验分页查询参数
        channelDto.checkParam();

        LambdaQueryWrapper<AdChannel> queryWrapper = new LambdaQueryWrapper<>();

        //如果传了名字 做模糊查询
        if (StringUtils.isNotBlank(channelDto.getName())) queryWrapper.like(AdChannel::getName, channelDto.getName());

        //分页查询
        IPage<AdChannel> page = new Page<>(channelDto.getPage(), channelDto.getSize());
        this.page(page, queryWrapper);

        return new PageResponseResult(channelDto.getPage(), channelDto.getSize(), (int) page.getTotal())
                .ok(page.getRecords());
    }

    @Override
    public ResponseResult insertChannel(AdChannel adChannel) {
        //检查参数
        if (adChannel == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);

        //设置创建时间
        adChannel.setCreatedTime(new Date());

        this.save(adChannel);

        return ResponseResult.okResult(null);
    }

    @Override
    public ResponseResult deleteChannel(Integer id) {
        //检查参数
        if (id == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);

        //查询数据是否存在
        AdChannel adChannel = this.getById(id);
        if (adChannel == null) return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);

        //判断数据是否有效 有效不能删除
        if (adChannel.getStatus()) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "频道有效, 不能删除");

        //删除数据
        this.removeById(id);

        return ResponseResult.okResult(null);
    }

    @Override
    public ResponseResult updateChannel(AdChannel adChannel) {
        //检查参数
        if (adChannel == null || adChannel.getId() == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        //查询数据是否存在
        if (this.getById(adChannel.getId()) == null) return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);

        //更新数据
        this.updateById(adChannel);
        return ResponseResult.okResult(null);

    }
}




