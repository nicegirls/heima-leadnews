package com.heima.admin.kafka.listener;

import com.heima.admin.service.WemediaNewsAutoScanService;
import com.heima.common.constants.NewsAutoScanConstants;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 自媒体文章自动审核监听器
 *
 * @author 韦东
 * @date 2021-09-08
 */
@Component
public class WemediaNewsAutoListener {

    @Resource
    private WemediaNewsAutoScanService wemediaNewsAutoScanService;

    @KafkaListener(topics = NewsAutoScanConstants.WM_NEWS_AUTO_SCAN_TOPIC)
    public void autoScan(ConsumerRecord<?, ?> record) {
        if (record != null) {
            Integer id = Integer.valueOf(record.value().toString());
            this.wemediaNewsAutoScanService.autoScanByWemediaId(id);
        }
    }
}
