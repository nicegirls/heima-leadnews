package com.heima.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.pojos.AdChannel;

/**
 * 频道服务mapper
 *
 * @author 韦东
 * @date 2021-08-30
 */
public interface AdChannelMapper extends BaseMapper<AdChannel> {

}




