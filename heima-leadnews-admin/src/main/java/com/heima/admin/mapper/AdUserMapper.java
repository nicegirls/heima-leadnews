package com.heima.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.pojos.AdUser;

/**
 * @Entity com.heima.admin.pojos.AdUser
 */
public interface AdUserMapper extends BaseMapper<AdUser> {

}




