package com.heima.admin;

import com.heima.admin.service.WemediaNewsAutoScanService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/8 周三
 */
@SpringBootTest(classes = AdminApplication.class)
@RunWith(SpringRunner.class)
public class AdminTest {

    @Resource
    private WemediaNewsAutoScanService wemediaNewsAutoScanService;

    @Test
    public void testScan() {

        this.wemediaNewsAutoScanService.autoScanByWemediaId(6223);
    }
}
