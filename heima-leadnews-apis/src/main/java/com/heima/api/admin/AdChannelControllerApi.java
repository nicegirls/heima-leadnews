package com.heima.api.admin;

import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;

/**
 * @author 韦东
 * @date 2021/8/30 周一
 */
@Api(value = "频道管理", tags = "channel")
public interface AdChannelControllerApi {

    /**
     * 根据频道名分页查询频道列表
     *
     * @param channelDto
     * @return {@link ResponseResult}
     */
    ResponseResult findByNameAndPage(ChannelDto channelDto);

    /**
     * 新增频道
     *
     * @param adChannel 频道
     * @return {@link ResponseResult}
     */
    ResponseResult insertChannel(AdChannel adChannel);

    /**
     * 删除频道
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    ResponseResult deleteChannel(Integer id);

    /**
     * 更新频道
     *
     * @param adChannel 频道
     * @return {@link ResponseResult}
     */
    ResponseResult updateChannel(AdChannel adChannel);


}
