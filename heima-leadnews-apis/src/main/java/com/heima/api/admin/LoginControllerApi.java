package com.heima.api.admin;

import com.heima.model.admin.pojos.AdUser;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;

/**
 * @author 韦东
 * @date 2021/9/1 周三
 */
@Api(tags = "login")
public interface LoginControllerApi {

    /**
     * 登录
     *
     * @param adUser (用户名+密码)
     * @return {@link ResponseResult}
     */
    ResponseResult login(AdUser adUser);
}
