package com.heima.api.admin;

import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;

/**
 * @author 韦东
 * @date 2021/9/1 周三
 */
@Api(tags = "sensitive")
public interface AdSensitiveControllerApi {

    /**
     * 根据名字分页查询
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    ResponseResult findByNameAndPage(SensitiveDto dto);

    /**
     * 插入
     *
     * @param adSensitive 敏感词
     * @return {@link ResponseResult}
     */
    ResponseResult insert(AdSensitive adSensitive);

    /**
     * 删除
     * @param id id
     * @return {@link ResponseResult}
     */
    ResponseResult delete(Integer id);

    /**
     * 更新
     *
     * @param adSensitive 广告敏感
     * @return {@link ResponseResult}
     */
    ResponseResult update(AdSensitive adSensitive);
}
