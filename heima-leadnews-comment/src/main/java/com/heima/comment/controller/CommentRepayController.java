package com.heima.comment.controller;

import com.heima.comment.service.CommentRepayService;
import com.heima.model.comment.dtos.CommentRepayDto;
import com.heima.model.comment.dtos.CommentRepayLikeDto;
import com.heima.model.comment.dtos.CommentRepaySaveDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/14 周二
 */
@RestController
@RequestMapping("api/v1/comment_repay")
public class CommentRepayController {

    @Resource
    private CommentRepayService commentRepayService;

    @PostMapping("load")
    public ResponseResult loadCommentRepay(@RequestBody CommentRepayDto dto) {
        return this.commentRepayService.loadCommentRepay(dto);
    }

    @PostMapping("save")
    public ResponseResult saveCommentRepay(@RequestBody CommentRepaySaveDto dto) {
        return this.commentRepayService.saveCommentRepay(dto);
    }

    @PostMapping("like")
    public ResponseResult saveCommentRepayLike(@RequestBody CommentRepayLikeDto dto) {
        return this.commentRepayService.saveCommentRepayLike(dto);
    }
}
