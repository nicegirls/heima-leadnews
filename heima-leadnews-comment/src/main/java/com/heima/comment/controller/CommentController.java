package com.heima.comment.controller;

import com.heima.comment.service.CommentService;
import com.heima.model.comment.dtos.CommentDto;
import com.heima.model.comment.dtos.CommentLikeDto;
import com.heima.model.comment.dtos.CommentSaveDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/14 周二
 */
@RestController
@RequestMapping("api/v1/comment")
public class CommentController {

    @Resource
    private CommentService commentService;

    @PostMapping("save")
    public ResponseResult saveComment(@RequestBody CommentSaveDto dto) {
        return this.commentService.saveComment(dto);
    }

    @PostMapping("load")
    public ResponseResult findByArticleId(@RequestBody CommentDto dto) {
        return this.commentService.findByArticleId(dto);
    }

    @PostMapping("like")
    public ResponseResult like(@RequestBody CommentLikeDto dto) {
        return this.commentService.like(dto);
    }

}
