package com.heima.comment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 韦东
 * @date 2021/9/14 周二
 */
@SpringBootApplication
@ComponentScan({
        "com.heima.comment",
        "com.heima.common"
})
@EnableDiscoveryClient
@ServletComponentScan
@EnableFeignClients
public class CommentApp {
    public static void main(String[] args) {
        SpringApplication.run(CommentApp.class, args);
    }
}
