package com.heima.comment.feign;

import com.heima.model.article.pojos.ApArticle;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 韦东
 * @date 2021/9/14 周二
 */
@FeignClient("leadnews-article")
@RequestMapping("api/v1/article")
public interface ArticleFeign {

    @GetMapping("one/{id}")
    public ApArticle findArticleById(@PathVariable Long id);
}
