package com.heima.comment.feign;

import com.heima.model.user.pojos.ApUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 韦东
 * @date 2021/9/14 周二
 */
@FeignClient("leadnews-user")
@RequestMapping("api/v1/user")
public interface UserFeign {

    @GetMapping("one/{id}")
    public ApUser findApUserById(@PathVariable Integer id);
}
