package com.heima.comment.service.impl;

import com.heima.comment.feign.ArticleFeign;
import com.heima.comment.feign.UserFeign;
import com.heima.comment.service.CommentService;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.comment.dtos.CommentDto;
import com.heima.model.comment.dtos.CommentLikeDto;
import com.heima.model.comment.dtos.CommentSaveDto;
import com.heima.model.comment.pojos.ApComment;
import com.heima.model.comment.pojos.ApCommentLike;
import com.heima.model.comment.vo.ApCommentVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 韦东
 * @date 2021/9/14 周二
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Resource
    private MongoTemplate mongoTemplate;

    @Resource
    private UserFeign userFeign;

    @Resource
    private GreenTextScan greenTextScan;

    @Resource
    private ArticleFeign articleFeign;

    @Override
    public ResponseResult saveComment(CommentSaveDto dto) {
        if (dto == null || dto.getContent() == null || dto.getArticleId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        if (dto.getContent().length() > 140) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "评论内容不能超过140字");
        }

        //判断是否登录
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //调用阿里云内容安全接口
        try {
            Map map = this.greenTextScan.greeTextScan(dto.getContent());
            if (!map.get("suggestion").equals("pass")) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "评论中有违规内容");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //查询用户头像 昵称
        user = this.userFeign.findApUserById(user.getId());
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "当前登录信息有误");
        }

        //查询文章信息获取频道
        //TODO 校验文章是否删除下架
        ApArticle article = this.articleFeign.findArticleById(dto.getArticleId());
        if (article == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "文章不存在或已下架");
        }

        //保存评论信息
        ApComment comment = new ApComment();
        comment.setAuthorId(user.getId());
        comment.setAuthorName(user.getName());
        comment.setEntryId(dto.getArticleId());
        comment.setChannelId(article.getChannelId());
        comment.setType((short) 0);
        comment.setContent(dto.getContent());
        comment.setImage(user.getImage());
        comment.setLikes(0);
        comment.setReply(0);
        comment.setFlag((short) 0);
        comment.setLongitude(new BigDecimal("0"));
        comment.setLatitude(new BigDecimal("0"));
        comment.setAddress("");
        comment.setOrd(0);
        comment.setCreatedTime(new Date());
        comment.setUpdatedTime(new Date());

        this.mongoTemplate.insert(comment);

        return ResponseResult.okResult(null);

    }

    @Override
    public ResponseResult findByArticleId(CommentDto dto) {
        //1.检查参数
        if (dto.getArticleId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (dto.getSize() == null || dto.getSize() == 0) {
            dto.setSize(20);
        }
        //按照文章id查询 分页 点赞数倒叙
        Query query = Query.query(Criteria.where("entryId").is(dto.getArticleId()).and("likes").lt(dto.getMinLikes()));
        query.limit(dto.getSize()).with(Sort.by(Sort.Direction.DESC, "likes"));
        List<ApComment> commentList = this.mongoTemplate.find(query, ApComment.class);
        //用户未登录
        if (AppThreadLocalUtils.getUser() == null) {
            return ResponseResult.okResult(commentList);
        }

        //用户已登陆 判断用户点赞了哪些评论
        List<String> idList = commentList.stream().map(comment -> comment.getId()).collect(Collectors.toList());
        Query query1 = Query.query(Criteria.where("commentId").in(idList).and("authorId").is(AppThreadLocalUtils.getUser().getId()));
        List<ApCommentLike> apCommentLikes = this.mongoTemplate.find(query1, ApCommentLike.class);

        ArrayList<ApCommentVo> result = new ArrayList<>();
        for (ApComment comment : commentList) {
            ApCommentVo commentVo = new ApCommentVo();
            BeanUtils.copyProperties(comment, commentVo);
            result.add(commentVo);
        }
        //如果有点赞的评论
        if (apCommentLikes != null) {
            for (ApCommentLike commentLike : apCommentLikes) {
                for (ApCommentVo commentVo : result) {
                    if (commentVo.getId().equals(commentLike.getCommentId())) {
                        commentVo.setOperation(commentLike.getOperation());
                        break;
                    }
                }
            }
        }

        return ResponseResult.okResult(result);
    }

    @Override
    public ResponseResult like(CommentLikeDto dto) {
        if (dto == null || dto.getCommentId() == null || dto.getOperation() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //判断是否登录
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //查询评论是否存在
        ApComment comment = this.mongoTemplate.findById(dto.getCommentId(), ApComment.class);
        if (comment == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "评论不存在");
        }

        //查询点赞信息文档
        Query query = Query.query(Criteria.where("authorId").is(user.getId()).and("commentId").is(dto.getCommentId()));
        ApCommentLike commentLike = this.mongoTemplate.findOne(query, ApCommentLike.class);

        //点赞操作
        if (dto.getOperation() == 0) {
            //更新点赞数量
            comment.setLikes(comment.getLikes() + 1);
            this.mongoTemplate.save(comment);

            if (commentLike == null) {
                //保存点赞信息
                commentLike = new ApCommentLike();
                commentLike.setAuthorId(user.getId());
                commentLike.setCommentId(dto.getCommentId());
            }
            commentLike.setOperation(dto.getOperation());
            this.mongoTemplate.save(commentLike);
        } else if (dto.getOperation() == 1) {//取消点赞
            //更新点赞数量
            comment.setLikes(comment.getLikes() > 0 ? comment.getLikes() - 1 : 0);
            this.mongoTemplate.save(comment);
            //更新点赞信息
            commentLike.setOperation(dto.getOperation());
            this.mongoTemplate.save(commentLike);
        }

        Map<String, Integer> map = new HashMap();
        map.put("likes", comment.getLikes());
        return ResponseResult.okResult(map);
    }
}
