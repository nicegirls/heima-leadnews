package com.heima.comment.service;

import com.heima.model.comment.dtos.CommentRepayDto;
import com.heima.model.comment.dtos.CommentRepayLikeDto;
import com.heima.model.comment.dtos.CommentRepaySaveDto;
import com.heima.model.common.dtos.ResponseResult;

/**
 * @author 韦东
 * @date 2021/9/14 周二
 */
public interface CommentRepayService {
    ResponseResult loadCommentRepay(CommentRepayDto dto);

    ResponseResult saveCommentRepay(CommentRepaySaveDto dto);

    ResponseResult saveCommentRepayLike(CommentRepayLikeDto dto);
}
