package com.heima.comment.service.impl;

import com.heima.comment.feign.ArticleFeign;
import com.heima.comment.feign.UserFeign;
import com.heima.comment.service.CommentRepayService;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.model.comment.dtos.CommentRepayDto;
import com.heima.model.comment.dtos.CommentRepayLikeDto;
import com.heima.model.comment.dtos.CommentRepaySaveDto;
import com.heima.model.comment.pojos.ApCommentRepay;
import com.heima.model.comment.pojos.ApCommentRepayLike;
import com.heima.model.comment.vo.ApCommentRepayVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 韦东
 * @date 2021/9/14 周二
 */
@Service
public class CommentRepayServiceImpl implements CommentRepayService {

    @Resource
    private MongoTemplate mongoTemplate;
    @Resource
    private UserFeign userFeign;
    @Resource
    private GreenTextScan greenTextScan;
    @Resource
    private ArticleFeign articleFeign;

    @Override
    public ResponseResult loadCommentRepay(CommentRepayDto dto) {
        //1.检查参数
        if (dto.getCommentId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (dto.getSize() == null || dto.getSize() == 0) {
            dto.setSize(20);
        }
        //2.按照评论id过滤，设置分页和排序
        Query query = Query.query(Criteria.where("commentId").is(dto.getCommentId()).and("likes").lt(dto.getMinLikes()));
        query.limit(dto.getSize()).with(Sort.by(Sort.Direction.DESC, "likes"));
        List<ApCommentRepay> list = this.mongoTemplate.find(query, ApCommentRepay.class);
        //用户未登录 加载数据
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.okResult(list);
        }

        //用户已登陆 判断用户点赞了哪些回复
        List<String> idList = list.stream().map(commentRepay -> commentRepay.getId()).collect(Collectors.toList());
        Query query1 = Query.query(Criteria.where("commentRepayId").in(idList).and("authorId").is(AppThreadLocalUtils.getUser().getId()));
        List<ApCommentRepayLike> apCommentRepayLikeList = this.mongoTemplate.find(query1, ApCommentRepayLike.class);

        ArrayList<ApCommentRepayVo> result = new ArrayList<>();
        for (ApCommentRepay commentRepay : list) {
            ApCommentRepayVo commentRepayVo = new ApCommentRepayVo();
            BeanUtils.copyProperties(commentRepay, commentRepayVo);
            result.add(commentRepayVo);
        }

        //如果有点赞的回复
        if (apCommentRepayLikeList != null) {
            for (ApCommentRepayLike commentRepayLike : apCommentRepayLikeList) {
                for (ApCommentRepayVo commentRepayVo : result) {
                    if (commentRepayVo.getId().equals(commentRepayLike.getCommentRepayId())) {
                        commentRepayVo.setOperation(commentRepayLike.getOperation());
                        break;
                    }
                }
            }
        }

        return ResponseResult.okResult(result);
    }

    @Override
    public ResponseResult saveCommentRepay(CommentRepaySaveDto dto) {
        if (dto == null || dto.getContent() == null || dto.getCommentId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        if (dto.getContent().length() > 140) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "回复内容不能超过140字");
        }

        //判断是否登录
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //调用阿里云内容安全接口
        try {
            Map map = this.greenTextScan.greeTextScan(dto.getContent());
            if (!map.get("suggestion").equals("pass")) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "评论中有违规内容");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //查询用户 昵称
        user = this.userFeign.findApUserById(user.getId());
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "当前登录信息有误");
        }

        //保存回复信息
        ApCommentRepay commentRepay = new ApCommentRepay();
        commentRepay.setAuthorId(user.getId());
        commentRepay.setAuthorName(user.getName());
        commentRepay.setCommentId(dto.getCommentId());
        commentRepay.setContent(dto.getContent());
        commentRepay.setLikes(0);
        commentRepay.setLongitude(new BigDecimal("0"));
        commentRepay.setLatitude(new BigDecimal("0"));
        commentRepay.setAddress("");
        commentRepay.setCreatedTime(new Date());
        commentRepay.setUpdatedTime(new Date());

        this.mongoTemplate.insert(commentRepay);

        return ResponseResult.okResult(null);

    }

    @Override
    public ResponseResult saveCommentRepayLike(CommentRepayLikeDto dto) {
        if (dto == null || dto.getCommentRepayId() == null || dto.getOperation() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //判断是否登录
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //查询回复是否存在
        ApCommentRepay commentRepay = this.mongoTemplate.findById(dto.getCommentRepayId(), ApCommentRepay.class);
        if (commentRepay == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "回复不存在");
        }

        //查询回复点赞信息文档
        Query query = Query.query(Criteria.where("authorId").is(user.getId()).and("commentRepayId").is(dto.getCommentRepayId()));
        ApCommentRepayLike commentRepayLike = this.mongoTemplate.findOne(query, ApCommentRepayLike.class);

        //点赞操作
        if (dto.getOperation() == 0) {
            //更新点赞数量
            commentRepay.setLikes(commentRepay.getLikes() + 1);
            this.mongoTemplate.save(commentRepay);

            if (commentRepayLike == null) {
                //保存点赞信息
                commentRepayLike = new ApCommentRepayLike();
                commentRepayLike.setAuthorId(user.getId());
                commentRepayLike.setCommentRepayId(dto.getCommentRepayId());
            }
            commentRepayLike.setOperation(dto.getOperation());
            this.mongoTemplate.save(commentRepayLike);
        } else if (dto.getOperation() == 1) {//取消点赞
            //更新点赞数量
            commentRepay.setLikes(commentRepay.getLikes() > 0 ? commentRepay.getLikes() - 1 : 0);
            this.mongoTemplate.save(commentRepay);
            //更新点赞信息
            commentRepayLike.setOperation(dto.getOperation());
            this.mongoTemplate.save(commentRepayLike);
        }

        Map<String, Integer> map = new HashMap();
        map.put("likes", commentRepay.getLikes());
        return ResponseResult.okResult(map);
    }
}
