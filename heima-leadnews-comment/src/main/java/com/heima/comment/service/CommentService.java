package com.heima.comment.service;

import com.heima.model.comment.dtos.CommentDto;
import com.heima.model.comment.dtos.CommentLikeDto;
import com.heima.model.comment.dtos.CommentSaveDto;
import com.heima.model.common.dtos.ResponseResult;

/**
 * @author 韦东
 * @date 2021/9/14 周二
 */
public interface CommentService {
    ResponseResult saveComment(CommentSaveDto dto);

    ResponseResult findByArticleId(CommentDto dto);

    ResponseResult like(CommentLikeDto dto);
}
