package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.wemedia.pojos.WmNewsMaterial;

/**
 * 新闻-素材 对应关系服务
 *
 * @author 韦东
 * @date 2021-09-04
 */
public interface WmNewsMaterialService extends IService<WmNewsMaterial> {

}
