package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmUser;

/**
 * 自媒体用户服务
 *
 * @author 韦东
 * @date 2021-09-02
 */
public interface WmUserService extends IService<WmUser> {

    /**
     * 登录
     *
     * @param wmUser wm用户
     * @return {@link ResponseResult}
     */
    ResponseResult login(WmUser wmUser);
}
