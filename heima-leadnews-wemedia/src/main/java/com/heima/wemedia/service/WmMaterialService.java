package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.MaterialDTO;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 */
public interface WmMaterialService extends IService<WmMaterial> {

    /**
     * 根据是否为收藏 分页查询素材列表
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    ResponseResult findByCollection(MaterialDTO dto);

    /**
     * 上传照片
     *
     * @param file 文件
     * @return {@link ResponseResult}
     */
    ResponseResult uploadPicture(MultipartFile file);

    /**
     * 删除图片
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    ResponseResult delPicture(Integer id);

    /**
     * 收藏/取消收藏
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    ResponseResult collect(Integer id);
}
