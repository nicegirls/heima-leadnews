package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.common.AppJwtUtil;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * 自媒体用户服务impl
 *
 * @author 韦东
 * @date 2021-09-02
 */
@Service
public class WmUserServiceImpl extends ServiceImpl<WmUserMapper, WmUser>
        implements WmUserService {

    @Override
    public ResponseResult login(WmUser wmUser) {
        //    检查参数
        if (wmUser == null || StringUtils.isBlank(wmUser.getName()) || StringUtils.isBlank(wmUser.getPassword()))
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE, "用户名或密码不能为空");

        //根据用户名查询数据库
        LambdaQueryWrapper<WmUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WmUser::getName, wmUser.getName());
        WmUser one = this.getOne(queryWrapper);

        //用户名不存在
        if (one == null) return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST, "用户名不存在");

        //对前台传来的密码使用数据库存储的盐进行加密
        String md5pw = DigestUtils.md5DigestAsHex((wmUser.getPassword() + one.getSalt()).getBytes(StandardCharsets.UTF_8));
        //判断密码是否正确
        if (!StringUtils.equals(md5pw, one.getPassword()))
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);

        //验证通过 返回token
        String token = AppJwtUtil.getToken(Long.valueOf(one.getId()));
        Map<String, Object> map = new HashMap<>();
        //屏蔽掉敏感信息
        one.setPassword(null);
        one.setSalt(null);

        map.put("user", one);
        map.put("token", token);

        return ResponseResult.okResult(map);
    }
}




