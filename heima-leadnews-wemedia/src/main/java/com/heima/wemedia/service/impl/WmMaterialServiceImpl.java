package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.fastdfs.FastDFSClient;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.MaterialDTO;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.threadlocal.WmThreadLocalUtils;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import com.heima.wemedia.service.WmNewsMaterialService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;

/**
 * 自媒体素材服务impl
 *
 * @author 韦东
 * @date 2021-09-04
 */
@Service
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialMapper, WmMaterial>
        implements WmMaterialService {

    @Resource
    FastDFSClient fastDFSClient;

    @Value("${fdfs.url}")
    private String fileServerUrl;

    @Resource
    private WmNewsMaterialService wmNewsMaterialService;

    @Override
    public ResponseResult findByCollection(MaterialDTO dto) {
        //检查参数
        if (dto == null || dto.getIsCollection() == null)
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        dto.checkParam();

        LambdaQueryWrapper<WmMaterial> queryWrapper = new LambdaQueryWrapper<>();
        //是否为收藏
        if (dto.getIsCollection() == 1) queryWrapper.eq(WmMaterial::getIsCollection, dto.getIsCollection());
        //查询当前用户的
        queryWrapper.eq(WmMaterial::getUserId, WmThreadLocalUtils.getUser().getId())
                //按照上传日期倒叙
                .orderByDesc(WmMaterial::getCreatedTime);
        //分页
        IPage<WmMaterial> page = new Page<>(dto.getPage(), dto.getSize());
        this.page(page, queryWrapper);

        //拼接图片url(数据库中存储的没有前缀)
        for (WmMaterial material : page.getRecords()) material.setUrl(this.fileServerUrl + material.getUrl());

        return new PageResponseResult(dto.getPage(), dto.getSize(), (int) page.getTotal()).ok(page.getRecords());
    }

    @Override
    public ResponseResult uploadPicture(MultipartFile file) {
        //检查参数
        if (file == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);

        String fileId = null;
        //上传文件
        try {
            fileId = this.fastDFSClient.uploadFile(file);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
        }
        //保存素材数据到表中 wm_material
        WmUser user = WmThreadLocalUtils.getUser();
        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setUserId(user.getId());
        wmMaterial.setUrl(fileId);
        wmMaterial.setIsCollection((byte) 0);
        wmMaterial.setCreatedTime(new Date());
        wmMaterial.setType((byte) 0);

        this.save(wmMaterial);
        //拼接图片url用于前台展示
        wmMaterial.setUrl(this.fileServerUrl + fileId);
        return ResponseResult.okResult(wmMaterial);
    }

    @Override
    public ResponseResult delPicture(Integer id) {
        //检查参数
        if (id == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        //查询图片是否存在
        WmMaterial wmMaterial = this.getById(id);
        if (wmMaterial == null) return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        //查询图片是否属于当前用户
        if (wmMaterial.getUserId() != WmThreadLocalUtils.getUser().getId())
            return ResponseResult.errorResult(AppHttpCodeEnum.NO_OPERATOR_AUTH, "不是你的 没权限");
        //查询素材是否被引用
        LambdaQueryWrapper<WmNewsMaterial> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WmNewsMaterial::getMaterialId, id);
        int count = this.wmNewsMaterialService.count(queryWrapper);
        if (count > 0)
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "当前图片被引用, 无法删除");

        //删除数据库数据
        this.removeById(id);
        //删除fastDFS中数据
        try {
            this.fastDFSClient.delFile(wmMaterial.getUrl().replace(this.fileServerUrl, ""));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
        }

        return ResponseResult.okResult(null);
    }

    @Override
    public ResponseResult collect(Integer id) {
        //检查参数
        if (id == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        //查询图片是否存在
        WmMaterial wmMaterial = this.getById(id);
        if (wmMaterial == null) return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        //查询图片是否属于当前用户
        if (!wmMaterial.getUserId().equals(WmThreadLocalUtils.getUser().getId()))
            return ResponseResult.errorResult(AppHttpCodeEnum.NO_OPERATOR_AUTH, "不是你的 没权限");

        //修改
        LambdaUpdateWrapper<WmMaterial> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(WmMaterial::getIsCollection, wmMaterial.getIsCollection() == 1 ? 0 : 1).eq(WmMaterial::getId, id);
        this.update(updateWrapper);

        return ResponseResult.okResult(null);
    }
}




