package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDTO;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageDTO;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.vo.WmNewsVO;

import java.util.List;

/**
 * @author 韦东
 * @date 2021-09-05
 */
public interface WmNewsService extends IService<WmNews> {

    /**
     * 根据条件分页查询
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    ResponseResult listCondition(WmNewsPageDTO dto);

    /**
     * 保存草稿/    新增/修改图文
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    ResponseResult draftOrSubmitNews(WmNewsDto dto);

    /**
     * 通过id查询
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    ResponseResult findById(Integer id);

    /**
     * 通过id删除
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    ResponseResult deleteById(Integer id);

    /**
     * 上架或下架
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    ResponseResult downOrUp(WmNewsDto dto);

    /**
     * 更新文章
     *
     * @param wmNews wm新闻
     * @return {@link ResponseResult}
     */
    ResponseResult updateWmNews(WmNews wmNews);

    /**
     * 查找所有到待发布日期的自媒体文章id
     *
     * @return {@link List}<{@link Integer}>
     */
    List<Integer> findReleaseable();

    /**
     * 模糊分页查询人工审核文章 包含作者信息
     *
     * @param dto dto
     * @return
     */
    ResponseResult findListWithTitleAndPage(NewsAuthDTO dto);

    /**
     * 查询自媒体文章信息 包含作者名
     *
     * @param id id
     * @return {@link WmNewsVO}
     */
    WmNewsVO FindNewsVO(Integer id);
}
