package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.constants.NewsAutoScanConstants;
import com.heima.common.constants.WemediaContans;
import com.heima.common.constants.WmNewsMessageConstants;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.NewsAuthDTO;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageDTO;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.model.wemedia.vo.WmNewsVO;
import com.heima.utils.threadlocal.WmThreadLocalUtils;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import com.heima.wemedia.service.WmNewsMaterialService;
import com.heima.wemedia.service.WmNewsService;
import com.heima.wemedia.service.WmUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author 韦东
 * @date 2021-09-05
 */
@Service
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews>
        implements WmNewsService {

    /**
     * 文件服务器的url
     */
    @Value("${fdfs.url}")
    private String fileServerURL;

    @Resource
    private WmNewsMaterialService wmNewsMaterialService;

    @Resource
    private WmNewsMaterialMapper wmNewsMaterialMapper;

    @Resource
    private WmMaterialService wmMaterialService;

    @Resource
    private KafkaTemplate kafkaTemplate;

    @Resource
    private WmNewsMapper wmNewsMapper;
    @Resource
    private WmUserService wmUserService;

    @Override
    public ResponseResult listCondition(WmNewsPageDTO dto) {
        //检查是否登录
        WmUser user = WmThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //检查参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        dto.checkParam();

        //构建查询参数
        LambdaQueryWrapper<WmNews> queryWrapper = new LambdaQueryWrapper<>();
        //关键字
        if (StringUtils.isNotBlank(dto.getKeyword())) {
            queryWrapper.like(WmNews::getTitle, dto.getKeyword());
        }
        //频道
        if (dto.getChannelId() != null) {
            queryWrapper.eq(WmNews::getChannelId, dto.getChannelId());
        }
        //状态
        if (dto.getStatus() != null) {
            queryWrapper.eq(WmNews::getStatus, dto.getStatus());
        }
        //日期范围
        if (dto.getBeginPubDate() != null && dto.getEndPubDate() != null && dto.getEndPubDate().after(dto.getBeginPubDate())) {
            queryWrapper.between(WmNews::getPublishTime, dto.getBeginPubDate(), dto.getEndPubDate());
        }
        //只查当前用户的
        queryWrapper.eq(WmNews::getUserId, user.getId());
        //按照发布日期倒序
        queryWrapper.orderByDesc(WmNews::getPublishTime);
        //分页
        IPage<WmNews> page = new Page<>(dto.getPage(), dto.getSize());
        this.page(page, queryWrapper);

        ResponseResult responseResult = new PageResponseResult(dto.getPage(), dto.getSize(), (int) page.getTotal()).ok(page.getRecords());
        responseResult.setHost(this.fileServerURL);
        return responseResult;
    }

    /**
     * 保存草稿 or  新增/修改图文
     * <p>
     * status=0 草稿
     * status = 1 提交
     * id =null 新增图文
     * id != null 修改图文
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    @Override
    public ResponseResult draftOrSubmitNews(WmNewsDto dto) {
        //校验是否登录
        WmUser user = WmThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //检查参数
        if (dto == null || StringUtils.isBlank(dto.getContent())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //拷贝数据
        WmNews wmNews = new WmNews();
        BeanUtils.copyProperties(dto, wmNews);
        //如果封面是自动 置为null
        if (wmNews.getType().equals(WemediaContans.WM_NEWS_TYPE_AUTO)) {
            wmNews.setType(null);
        }
        //处理前台传来的沙雕数据  标题图片 "null,null,null"
        String images = String.join(",", dto.getImages()).replace(this.fileServerURL, "");
        if ("null,null,null".equals(images)) {
            dto.setImages(null);
            images = null;
        }
        wmNews.setImages(images);
        //设置当前用户id
        wmNews.setUserId(user.getId());

        //新增或修改
        this.saveOrUpdateWmNews(wmNews);

        //关联图文与素材的关系
        //.1关联正文图片
        // .1.1提取正文中的图片信息
        List<String> materialURLs = this.extractMaterials(wmNews.getContent());
        Integer wmNewsId = wmNews.getId();
        this.materials2NewsWithContent(wmNewsId, materialURLs);

        //.2关联标题图片
        //.2.1如果是自动
        if (dto.getType().equals(WemediaContans.WM_NEWS_TYPE_AUTO)) {
            this.autoAaterials2NewsWithCover(wmNewsId, materialURLs);
        } else {//.2.2不是自动
            this.materials2NewsWithCover(wmNewsId, Arrays.asList(images.split(",")));
        }
        return ResponseResult.okResult(null);
    }

    /**
     * 查询通过id
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    @Override
    public ResponseResult findById(Integer id) {
        //1.参数检查
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "文章Id不可缺少");
        }
        //2.查询数据
        WmNews wmNews = this.getById(id);
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章不存在");
        }

        //3.结果返回
        ResponseResult responseResult = ResponseResult.okResult(wmNews);
        responseResult.setHost(this.fileServerURL);
        return responseResult;
    }

    /**
     * 删除通过id
     *
     * @param id id
     * @return {@link ResponseResult}
     */
    @Override
    public ResponseResult deleteById(Integer id) {
        //检查参数
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "id不能为空");
        }

        //查询文章是否存在
        WmNews wmNews = this.getById(id);
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //如果已发布 并且上架中 不能删除
        if (wmNews.getStatus().equals(WmNews.Status.PUBLISHED.getCode()) && wmNews.getEnable().equals(WemediaContans.WM_NEWS_ENABLE_UP)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "文章已发布, 不能删除");
        }

        //删除素材与文章关联关系
        LambdaQueryWrapper<WmNewsMaterial> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WmNewsMaterial::getNewsId, id);
        this.wmNewsMaterialService.remove(queryWrapper);

        //删除文章
        this.removeById(id);

        return ResponseResult.okResult(null);

    }

    /**
     * 上下架
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    @Override
    public ResponseResult downOrUp(WmNewsDto dto) {
        //检查参数
        if (dto == null || dto.getId() == null || dto.getEnable() == null || !(dto.getEnable() == 0 || dto.getEnable() == 1)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询数据是否存在
        WmNews wmNews = this.getById(dto.getId());
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //判断数据是否发布
        if (!wmNews.getStatus().equals(WmNews.Status.PUBLISHED.getCode())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "文章没有发布, 不能上下架");
        }

        //上下架
        LambdaUpdateWrapper<WmNews> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(WmNews::getEnable, dto.getEnable()).eq(WmNews::getId, dto.getId());
        this.update(updateWrapper);

        //同步app端article状态
        Map map = new HashMap();
        map.put("enable", wmNews.getEnable());
        map.put("articleId", wmNews.getArticleId());
        this.kafkaTemplate.send(WmNewsMessageConstants.WM_NEWS_UP_OR_DOWN_TOPIC, JSON.toJSONString(map));

        return ResponseResult.okResult(null);
    }

    @Override
    public ResponseResult updateWmNews(WmNews wmNews) {

        //检查参数
        if (wmNews == null || wmNews.getId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        this.updateById(wmNews);

        return ResponseResult.okResult(null);
    }

    @Override
    public List<Integer> findReleaseable() {
        LambdaQueryWrapper<WmNews> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WmNews::getStatus, 8).le(WmNews::getPublishTime, new Date()).select(WmNews::getId);
        return this.listObjs(queryWrapper, new Function<Object, Integer>() {
            @Override
            public Integer apply(Object o) {
                return (Integer) o;
            }
        });
    }

    @Override
    public ResponseResult findListWithTitleAndPage(NewsAuthDTO dto) {
        dto.checkParam();
        Integer start = (dto.getPage() - 1) * dto.getSize();
        Integer num = dto.getSize();
        String title = dto.getTitle();
        //模糊分页查询自媒体文章列表 包含作者名
        List<WmNewsVO> wmNewsVOList = this.wmNewsMapper.findListWithTitleAndPage(start, num, title);
        //查询总条数
        LambdaQueryWrapper<WmNews> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(title)) {
            queryWrapper.like(WmNews::getTitle, title);
        }
        int count = this.count(queryWrapper);

        return new PageResponseResult(dto.getPage(), dto.getSize(), count).ok(wmNewsVOList);
    }

    @Override
    public WmNewsVO FindNewsVO(Integer id) {
        WmNewsVO wmNewsVO = new WmNewsVO();

        //查询文章
        WmNews wmNews = this.getById(id);
        //文章不存在直接返回
        if (wmNews == null) {
            return null;
        }
        //查询作者
        if (wmNews.getUserId() != null) {
            WmUser wmUser = this.wmUserService.getById(wmNews.getUserId());
            //作者存在
            if (wmUser != null) {
                wmNewsVO.setAuthorName(wmUser.getName());
            }
        }
        //复制属性
        BeanUtils.copyProperties(wmNews, wmNewsVO);
        return wmNewsVO;
    }

    /**
     * 从正文中自动关联素材到图文
     *
     * @param wmNewsId     图文id
     * @param materialURLs 正文中的素材url集合
     */
    private void autoAaterials2NewsWithCover(Integer wmNewsId, List<String> materialURLs) {

        Byte type;
        //正文中图片小于等于2
        if (materialURLs.size() <= 2) {
            //从正文中获取1张图片
            materialURLs = materialURLs.stream().limit(1).collect(Collectors.toList());
            //设置标题类型为单图
            type = WemediaContans.WM_NEWS_ONE_IMAGE;
        } else if (materialURLs.size() > 2) {//正文中图片大于3张
            //从正文中获取3张图片
            materialURLs = materialURLs.stream().limit(3).collect(Collectors.toList());
            //设置标题类型为多图
            type = WemediaContans.WM_NEWS_MANY_IMAGE;
        } else { //正文中没有图片
            //设置标题类型为无图
            type = WemediaContans.WM_NEWS_NONE_IMAGE;
        }

        //更新db中wmnews的type和images字段
        LambdaUpdateWrapper<WmNews> updateWrapper = new LambdaUpdateWrapper<>();

        if (materialURLs.size() > 0) {
            //如果正文中有图 关联关系
            this.associateMaterials2News(wmNewsId, materialURLs, WemediaContans.WM_COVER_REFERENCE);

            String images = materialURLs.stream().collect(Collectors.joining(","));
            updateWrapper.set(WmNews::getImages, images);
        }
        updateWrapper.set(WmNews::getType, type).eq(WmNews::getId, wmNewsId);
        this.update(updateWrapper);
    }

    /**
     * 关联标题中的素材和图文对应关系()
     *
     * @param wmNewsId     图文id
     * @param materialURLs 标题中的素材url集合
     */
    private void materials2NewsWithCover(Integer wmNewsId, List<String> materialURLs) {
        this.associateMaterials2News(wmNewsId, materialURLs, WemediaContans.WM_COVER_REFERENCE);
    }

    /**
     * 关联正文中的素材和图文对应关系
     *
     * @param wmNewsId     图文id
     * @param materialURLs 正文中的素材url集合
     */
    private void materials2NewsWithContent(Integer wmNewsId, List<String> materialURLs) {
        this.associateMaterials2News(wmNewsId, materialURLs, WemediaContans.WM_CONTENT_REFERENCE);
    }

    /**
     * 关联素材和图文对应关系
     *
     * @param wmNewsId       图文
     * @param materialURLs   素材的url集合
     * @param contentOrCover 正文或标题
     */
    private void associateMaterials2News(Integer wmNewsId, List<String> materialURLs, Byte contentOrCover) {
        // .1关联正文中的素材和图文的关系
        if (!CollectionUtils.isEmpty(materialURLs)) {
            List<Integer> materialIds = this.getIdsByURLs(materialURLs);
            //设置关联关系
            this.wmNewsMaterialMapper.saveRelationsByContent(materialIds, wmNewsId, contentOrCover);
        }
    }

    /**
     * 根据url查询素材id
     *
     * @param materialURLs 素材URL集合(不含前缀)
     * @return {@link List}<{@link Integer}>
     */
    private List<Integer> getIdsByURLs(List<String> materialURLs) {
        LambdaQueryWrapper<WmMaterial> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(WmMaterial::getUrl, materialURLs).select(WmMaterial::getId);
        List<Integer> materialIds = this.wmMaterialService.listObjs(queryWrapper, new Function<Object, Integer>() {
            @Override
            public Integer apply(Object o) {
                return Integer.valueOf(o.toString());
            }
        });

        return materialIds;
    }

    /**
     * 提取图片信息 (返回不含前缀)
     *
     * @param content 图文内容
     * @return {@link List}<{@link String}>
     */
    private List<String> extractMaterials(String content) {
        List<Map> maps = JSON.parseArray(content, Map.class);
        List<String> materials = new ArrayList<>();
        for (Map map : maps) {
            if (map.get("type").equals(WemediaContans.WM_NEWS_TYPE_IMAGE)) {
                materials.add(((String) map.get("value")).replace(this.fileServerURL, ""));
            }
        }
        return materials;
    }

    /**
     * 保存或修改图文
     *
     * @param wmNews 图文
     */
    private void saveOrUpdateWmNews(WmNews wmNews) {
        //如果是新增 设置创建时间
        if (wmNews.getId() == null) {
            wmNews.setCreatedTime(new Date());
        } else {//如果是修改 删除原有素材和图文对应关系
            LambdaQueryWrapper<WmNewsMaterial> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(WmNewsMaterial::getNewsId, wmNews.getId());
            this.wmNewsMaterialService.remove(queryWrapper);
        }
        //设置提交时间
        wmNews.setSubmitedTime(new Date());

        //如果是提交
        if (wmNews.getStatus().equals(WmNews.Status.SUBMIT.getCode())) {
            //设置默认上架
            wmNews.setEnable(WemediaContans.WM_NEWS_ENABLE_UP);
        }
        //修改或保存

        if (wmNews.getPublishTime() == null) {
            wmNews.setPublishTime(new Date());
        }

        //保存或修改成功后, 发消息自动审核
        boolean isSuccess = this.saveOrUpdate(wmNews);
        if (isSuccess) {
            this.kafkaTemplate.send(NewsAutoScanConstants.WM_NEWS_AUTO_SCAN_TOPIC, wmNews.getId().toString());
        }
    }
}




