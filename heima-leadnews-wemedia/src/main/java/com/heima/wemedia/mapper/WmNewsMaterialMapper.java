package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.heima.model.wemedia.pojos.WmNewsMaterial
 */
public interface WmNewsMaterialMapper extends BaseMapper<WmNewsMaterial> {
    /**
     * 保存素材与图文的关系
     *
     * @param materials 素材id集合
     * @param newId     图文id
     * @param type      引用类型 0 内容引用  1 主图引用
     */
    void saveRelationsByContent(@Param("materials") List<Integer> materials, @Param("newsId") Integer newId, @Param("type") int type);
}




