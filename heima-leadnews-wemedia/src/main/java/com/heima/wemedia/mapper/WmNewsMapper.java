package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.vo.WmNewsVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * WmNews映射器
 *
 * @author 韦东
 * @date 2021-09-05
 */
public interface WmNewsMapper extends BaseMapper<WmNews> {

    /**
     * 根据标题模糊分页查询自媒体文章
     *
     * @param start 查询起始
     * @param num   查询数量
     * @param title 标题
     * @return {@link List}<{@link WmNewsVO}>
     */
    List<WmNewsVO> findListWithTitleAndPage(@Param("start") Integer start, @Param("num") Integer num, @Param("title") String title);
}




