package com.heima.wemedia;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 韦东
 * @date 2021/8/30 周一
 */
@SpringBootApplication
@MapperScan("com.heima.wemedia.mapper")
@ComponentScan({
        "com.heima.wemedia",
        "com.heima.common",
        "com.heima.seata.config"
})
@ServletComponentScan
public class WemediaApp {

    public static void main(String[] args) {
        SpringApplication.run(WemediaApp.class, args);
    }

    /**
     * mybatis-plus分页插件拦截器
     *
     * @return {@link PaginationInterceptor}
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
