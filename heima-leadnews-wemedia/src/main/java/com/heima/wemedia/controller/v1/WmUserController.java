package com.heima.wemedia.controller.v1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.wemedia.service.WmUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/2 周四
 */
@RestController
@RequestMapping("api/v1/user")
public class WmUserController {

    @Resource
    private WmUserService wmUserService;

    @GetMapping("findByName/{name}")
    public ResponseResult findByName(@PathVariable String name){
        //检查参数
        if (StringUtils.isBlank(name)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //根据名称查询
        LambdaQueryWrapper<WmUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WmUser::getName, name);
        WmUser wmUser = this.wmUserService.getOne(queryWrapper);

        return ResponseResult.okResult(wmUser);
    }

    @PostMapping("save")
    public ResponseResult save(@RequestBody WmUser wmUser) {
        this.wmUserService.save(wmUser);
        return ResponseResult.okResult(null);
    }

    @GetMapping("findById/{id}")
    public WmUser findById(@PathVariable Integer id) {
        return this.wmUserService.getById(id);
    }
}
