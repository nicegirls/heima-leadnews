package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.MaterialDTO;
import com.heima.wemedia.service.WmMaterialService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/4 周六
 */
@RestController
@RequestMapping("api/v1/material")
public class WmMaterialController {

    @Resource
    private WmMaterialService wmMaterialService;

    @GetMapping("del_picture/{id}")
    public ResponseResult delPicture(@PathVariable Integer id) {
        return this.wmMaterialService.delPicture(id);
    }

    @PostMapping("list")
    public ResponseResult findByCollection(@RequestBody MaterialDTO dto) {
        return this.wmMaterialService.findByCollection(dto);
    }

    @PostMapping("upload_picture")
    public ResponseResult uploadPicture(MultipartFile multipartFile) {
        return this.wmMaterialService.uploadPicture(multipartFile);
    }

    @GetMapping("collect/{id}")
    public ResponseResult collect(@PathVariable Integer id) {
        return this.wmMaterialService.collect(id);

    }
}
