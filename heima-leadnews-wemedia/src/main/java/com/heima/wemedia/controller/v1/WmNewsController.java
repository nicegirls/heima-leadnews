package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDTO;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageDTO;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.vo.WmNewsVO;
import com.heima.wemedia.service.WmNewsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 韦东
 * @date 2021/9/5 周日
 */
@RestController
@RequestMapping("api/v1/news")
public class WmNewsController {

    @Resource
    private WmNewsService wmNewsService;

    @PostMapping("list")
    public ResponseResult listCondition(@RequestBody WmNewsPageDTO dto) {
        return this.wmNewsService.listCondition(dto);
    }

    @PostMapping("submit")
    public ResponseResult draftOrSubmitNews(@RequestBody WmNewsDto dto) {
        return this.wmNewsService.draftOrSubmitNews(dto);
    }

    @GetMapping("one/{id}")
    public ResponseResult findById(@PathVariable Integer id) {
        return this.wmNewsService.findById(id);
    }

    @GetMapping("findOne/{id}")
    public WmNews getWmNewsById(@PathVariable Integer id) {
        return this.wmNewsService.getById(id);
    }

    @GetMapping("del_news/{id}")
    public ResponseResult deleteById(@PathVariable Integer id) {
        return this.wmNewsService.deleteById(id);
    }

    @PostMapping("down_or_up")
    public ResponseResult downOrUp(@RequestBody WmNewsDto dto) {
        return this.wmNewsService.downOrUp(dto);
    }

    @PostMapping("update")
    public ResponseResult updateWmNews(@RequestBody WmNews wmNews) {
        return this.wmNewsService.updateWmNews(wmNews);
    }

    @GetMapping("findReleaseable")
    public List<Integer> findReleasedable() {
        return this.wmNewsService.findReleaseable();
    }

    @PostMapping("findListWithTitleAndPage")
    public ResponseResult findListWithTitleAndPage(@RequestBody NewsAuthDTO dto) {
        return this.wmNewsService.findListWithTitleAndPage(dto);
    }

    @GetMapping("find_news_vo/{id}")
    public WmNewsVO FindNewsVO(@PathVariable Integer id) {
        return this.wmNewsService.FindNewsVO(id);
    }
}
