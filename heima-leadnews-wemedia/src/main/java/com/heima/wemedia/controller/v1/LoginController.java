package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.wemedia.service.WmUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/1 周三
 */
@RestController
@RequestMapping("login")
public class LoginController {

    @Resource
    private WmUserService wmUserService;

    @PostMapping("in")
    public ResponseResult login(@RequestBody WmUser wmUser) {
        return this.wmUserService.login(wmUser);
    }
}
