package com.heima.user.mapper;

import com.heima.model.user.pojos.ApUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.heima.model.user.pojos.ApUser
 */
public interface ApUserMapper extends BaseMapper<ApUser> {

}




