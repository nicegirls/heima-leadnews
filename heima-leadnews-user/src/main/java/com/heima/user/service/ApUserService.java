package com.heima.user.service;

import com.heima.model.user.pojos.ApUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface ApUserService extends IService<ApUser> {

}
