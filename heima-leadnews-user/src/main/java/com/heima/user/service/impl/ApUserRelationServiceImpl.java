package com.heima.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.common.constants.FollowBehaviorConstants;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.behavior.dtos.FollowBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.UserRelationDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserFan;
import com.heima.model.user.pojos.ApUserFollow;
import com.heima.user.feign.ArticleFeignClient;
import com.heima.user.mapper.ApUserFanMapper;
import com.heima.user.mapper.ApUserFollowMapper;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserRelationService;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author 韦东
 * @date 2021/9/11 周六
 */
@Service
public class ApUserRelationServiceImpl implements ApUserRelationService {

    @Resource
    private ArticleFeignClient articleFeignClient;
    @Resource
    private ApUserMapper apUserMapper;
    @Resource
    private ApUserFollowMapper apUserFollowMapper;
    @Resource
    private ApUserFanMapper apUserFanMapper;
    @Resource
    private KafkaTemplate kafkaTemplate;

    @Override
    public ResponseResult follow(UserRelationDto dto) {
        //检查参数
        if (dto == null || dto.getAuthorId() == null || dto.getArticleId() == null || dto.getOperation() > 1 || dto.getOperation() < 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //粉丝信息
        ApUser apUser = AppThreadLocalUtils.getUser();
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        Integer fanId = apUser.getId();
        ApUser fan = this.apUserMapper.selectById(fanId);
        if (fan == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.TOKEN_INVALID, "登录信息有误");
        }

        //查询作者信息
        ApAuthor apAuthor = this.articleFeignClient.findAuthorById(dto.getAuthorId());
        if (apAuthor == null || apAuthor.getUserId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "作者不存在");
        }
        Integer followId = apAuthor.getUserId();

        //关注
        if (dto.getOperation() == 0) {
            return this.followByUserId(followId, fan, dto.getArticleId());
        } else {//取消关注
            return this.cancelFollowByUserId(followId, fanId);
        }
    }

    /**
     * 取消关注
     *
     * @param followId 被关注者id
     * @param fanId    粉丝id
     * @return {@link ResponseResult}
     */
    private ResponseResult cancelFollowByUserId(Integer followId, Integer fanId) {
        ApUserFollow apUserFollow = this.apUserFollowMapper.selectOne(Wrappers.<ApUserFollow>lambdaQuery().eq(ApUserFollow::getUserId, fanId).eq(ApUserFollow::getFollowId, followId));
        if (apUserFollow == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "未曾关注");
        }
        this.apUserFollowMapper.deleteById(apUserFollow.getId());

        ApUserFan apUserFan = this.apUserFanMapper.selectOne(Wrappers.<ApUserFan>lambdaQuery().eq(ApUserFan::getUserId, followId).eq(ApUserFan::getFansId, fanId));
        if (apUserFan != null) {
            this.apUserFanMapper.deleteById(apUserFan.getId());
        }

        return ResponseResult.okResult(null);
    }

    /**
     * 关注
     *
     * @param followId  被关注用户id
     * @param fan       粉丝
     * @param articleId 文章的id
     * @return {@link ResponseResult}
     */
    private ResponseResult followByUserId(Integer followId, ApUser fan, Long articleId) {
        ApUser follow = this.apUserMapper.selectById(followId);
        if (follow == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN, "被关注用户不存在");
        }

        ApUserFollow apUserFollow = this.apUserFollowMapper.selectOne(Wrappers.<ApUserFollow>lambdaQuery().eq(ApUserFollow::getUserId, fan.getId()).eq(ApUserFollow::getFollowId, followId));
        if (apUserFollow != null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST, "已关注");
        }
        //保存app用户关注信息
        apUserFollow = new ApUserFollow();
        apUserFollow.setUserId(fan.getId());
        apUserFollow.setFollowId(followId);
        apUserFollow.setFollowName(follow.getName());
        apUserFollow.setCreatedTime(new Date());
        apUserFollow.setIsNotice(true);
        apUserFollow.setLevel((short) 1);
        this.apUserFollowMapper.insert(apUserFollow);

        ApUserFan apUserFan = this.apUserFanMapper.selectOne(Wrappers.<ApUserFan>lambdaQuery().eq(ApUserFan::getUserId, followId).eq(ApUserFan::getFansId, fan.getId()));
        if (apUserFan == null) {
            //保存app用户粉丝信息
            apUserFan = new ApUserFan();
            apUserFan.setUserId(followId);
            apUserFan.setFansId(fan.getId().longValue());
            apUserFan.setFansName(fan.getName());
            apUserFan.setLevel((short) 0);
            apUserFan.setCreatedTime(new Date());
            apUserFan.setIsDisplay(true);
            apUserFan.setIsShieldLetter(false);
            apUserFan.setIsShieldComment(false);
            this.apUserFanMapper.insert(apUserFan);
        }

        //发消息记录用户关注行为
        FollowBehaviorDto dto = new FollowBehaviorDto();
        dto.setArticleId(articleId);
        dto.setFollowId(followId);
        dto.setUserId(fan.getId());
        this.kafkaTemplate.send(FollowBehaviorConstants.FOLLOW_BEHAVIOR_TOPIC, JSON.toJSONString(dto));

        return ResponseResult.okResult(null);
    }
}
