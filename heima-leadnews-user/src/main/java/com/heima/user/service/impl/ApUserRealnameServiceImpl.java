package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.constants.UserConstans;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.AuthDTO;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserRealname;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.user.feign.ArticleFeignClient;
import com.heima.user.feign.WeMediaFeignClient;
import com.heima.user.mapper.ApUserRealnameMapper;
import com.heima.user.service.ApUserRealnameService;
import com.heima.user.service.ApUserService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 用户认证服务impl
 *
 * @author 韦东
 * @date 2021-09-02
 */
@Service
@Transactional
public class ApUserRealnameServiceImpl extends ServiceImpl<ApUserRealnameMapper, ApUserRealname>
        implements ApUserRealnameService {

    @Resource
    private WeMediaFeignClient weMediaFeignClient;

    @Resource
    private ArticleFeignClient articleFeignClient;

    @Resource
    private ApUserService apUserService;

    @Override
    public ResponseResult loadListByStatus(AuthDTO dto) {
        //检查参数
        if (dto == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        dto.checkParam();

        LambdaQueryWrapper<ApUserRealname> queryWrapper = new LambdaQueryWrapper<>();
        //添加状态查询参数
        if (dto.getStatus() != null) queryWrapper.eq(ApUserRealname::getStatus, dto.getStatus());
        //分页查询
        IPage<ApUserRealname> page = new Page<>(dto.getPage(), dto.getSize());
        this.page(page, queryWrapper);
        //构建返回数据
        return new PageResponseResult(dto.getPage(), dto.getSize(), (int) page.getTotal()).ok(page.getRecords());
    }

    @GlobalTransactional
    @Override
    public ResponseResult updateStatusById(AuthDTO dto, Byte status) {
        //检查参数
        if (dto == null || dto.getId() == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);

        //判断用户是否存在
        ApUserRealname userRealname = this.getById(dto.getId());
        ApUser apUser = this.apUserService.getById(userRealname.getUserId());
        if (apUser == null) return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);

        //修改认证信息状态
        LambdaUpdateWrapper<ApUserRealname> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(ApUserRealname::getStatus, status).eq(ApUserRealname::getId, dto.getId());
        //如果是驳回 设置原因
        if (StringUtils.isNotBlank(dto.getMsg())) updateWrapper.set(ApUserRealname::getReason, dto.getMsg());
        this.update(updateWrapper);

        //如果是审核通过, 同步数据到 自媒体模块用户表  文章模块作者表
        if (status.equals(UserConstans.PASS_AUTH)) {
            ResponseResult result = this.createWmUserAndAuthor(apUser);
            //result不为null 代表远程调用出现错误 返回错误信息
            if (result != null) return result;
        }else {//TODO 审核不通过 查询自媒体模块用户表  如果有记录 修改状态为不可用

        }
        //int a = 1 / 0;
        return ResponseResult.okResult(null);
    }

    private ResponseResult createWmUserAndAuthor(ApUser apUser) {
        //根据用户名查询自媒体用户信息
        ResponseResult weMediaResult = this.weMediaFeignClient.findByName(apUser.getName());
        //返回状态码不为0 远程调用出现错误
        if (weMediaResult.getCode() != 0) return weMediaResult;
        //自媒体用户信息不存在     创建
        if (weMediaResult.getData() == null) {
            WmUser wmUser = new WmUser();
            wmUser.setApUserId(apUser.getId());
            wmUser.setName(apUser.getName());
            wmUser.setPassword(apUser.getPassword());
            wmUser.setSalt(apUser.getSalt());
            wmUser.setImage(apUser.getImage());
            wmUser.setPhone(apUser.getPhone());
            wmUser.setStatus((byte) 9);
            wmUser.setCreatedTime(new Date());
            ResponseResult save = this.weMediaFeignClient.save(wmUser);
            //返回状态码不为0 远程调用出现错误
            if (save.getCode() != 0) return save;
        }

        //创建作者信息
        ResponseResult result = this.createAuthor(apUser);
        //返回信息不为null 代表方法执行出现问题 返回错误信息
        if (result != null) return result;

        //修改社交帐号(ap_user)类型(flag)  1代表自媒体人
        apUser.setFlag((byte) 1);
        this.apUserService.updateById(apUser);
        return null;
    }

    private ResponseResult createAuthor(ApUser apUser) {
        //根据ap_user_id查询作者信息
        ResponseResult articleFeignResult = this.articleFeignClient.findByUserId(apUser.getId());
        //返回状态码不为0 表示远程调用出现错误 返回错误信息
        if (articleFeignResult.getCode() != 0) return articleFeignResult;
        //对应作者信息不存在  创建
        if (articleFeignResult.getData() == null) {
            ApAuthor author = new ApAuthor();
            author.setName(apUser.getName());
            author.setType(UserConstans.AUTH_TYPE);
            author.setUserId(apUser.getId());
            author.setCreatedTime(new Date());

            ResponseResult save = this.articleFeignClient.save(author);
            //返回状态码不为0 表示远程调用出现错误 返回错误信息
            if (save.getCode() != 0) return save;
        }
        return null;
    }
}




