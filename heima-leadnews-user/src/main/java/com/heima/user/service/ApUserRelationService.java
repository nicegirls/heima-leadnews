package com.heima.user.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.UserRelationDto;

/**
 * @author 韦东
 * @date 2021/9/11 周六
 */
public interface ApUserRelationService {
    /**
     * 用户关注/取消关注
     *
     * @param dto
     * @return
     */
    public ResponseResult follow(UserRelationDto dto);

}
