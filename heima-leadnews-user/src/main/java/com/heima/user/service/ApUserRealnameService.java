package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.AuthDTO;
import com.heima.model.user.pojos.ApUserRealname;

/**
 * 用户认证服务
 *
 * @author 韦东
 * @date 2021-09-02
 */
public interface ApUserRealnameService extends IService<ApUserRealname> {

    /**
     * 根据状态查询认证列表
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    ResponseResult loadListByStatus(AuthDTO dto);

    /**
     * 更新状态通过id
     *
     * @param dto      dto
     * @param status 状态
     * @return {@link ResponseResult}
     */
    ResponseResult updateStatusById(AuthDTO dto, Byte status);
}
