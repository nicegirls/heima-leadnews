package com.heima.user.controller.v1;

import com.heima.common.constants.UserConstans;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.AuthDTO;
import com.heima.user.service.ApUserRealnameService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/2 周四
 */
@RestController
@RequestMapping("api/v1/auth")
public class ApUserRealnameController {

    @Resource
    private ApUserRealnameService apUserRealnameService;

    @PostMapping("list")
    public ResponseResult loadListByStatus(@RequestBody AuthDTO dto){
       return this.apUserRealnameService.loadListByStatus(dto);
    }

    @PostMapping("authPass")
    public ResponseResult authPass(AuthDTO dto) {
        return this.apUserRealnameService.updateStatusById(dto, UserConstans.PASS_AUTH);
    }

    @PostMapping("authFail")
    public ResponseResult authFail(AuthDTO dto) {
        return this.apUserRealnameService.updateStatusById(dto, UserConstans.FAIL_AUTH);
    }
}
