package com.heima.user.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.user.service.ApUserLoginService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/10 周五
 */
@RestController
@RequestMapping("api/v1/login")
public class ApUserLoginController {

    @Resource
    private ApUserLoginService apUserLoginService;

    @PostMapping("login_auth")
    public ResponseResult login(@RequestBody LoginDto dto) {
        return this.apUserLoginService.login(dto);
    }
}