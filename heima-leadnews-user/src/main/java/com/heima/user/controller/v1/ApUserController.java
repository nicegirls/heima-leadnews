package com.heima.user.controller.v1;

import com.heima.model.user.pojos.ApUser;
import com.heima.user.service.ApUserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/8 周三
 */
@RestController
@RequestMapping("api/v1/user")
public class ApUserController {

    @Resource
    private ApUserService apUserService;

    @GetMapping("one/{id}")
    public ApUser findById(@PathVariable Integer id) {
        return this.apUserService.getById(id);
    }

}
