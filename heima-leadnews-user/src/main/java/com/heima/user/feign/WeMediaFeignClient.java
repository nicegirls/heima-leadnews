package com.heima.user.feign;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 自媒体服务feign客户端
 *
 * @author 韦东
 * @date 2021-09-02
 */
@FeignClient("leadnews-wemedia")
@RequestMapping("api/v1/user")
public interface WeMediaFeignClient {

    @GetMapping("findByName/{name}")
    public ResponseResult findByName(@PathVariable String name);

    @PostMapping("save")
    public ResponseResult save(@RequestBody WmUser wmUser);
}
