package com.heima.user.feign;

import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 文章服务feign客户端
 *
 * @author 韦东
 * @date 2021-09-02
 */
@FeignClient("leadnews-article")
@RequestMapping("api/v1/author")
public interface ArticleFeignClient {

    @GetMapping("findByUserId/{userId}")
    public ResponseResult findByUserId(@PathVariable Integer userId);

    @PostMapping("save")
    public ResponseResult save(@RequestBody ApAuthor author);

    @GetMapping("one/{id}")
    public ApAuthor findAuthorById(@PathVariable Integer id);
}
