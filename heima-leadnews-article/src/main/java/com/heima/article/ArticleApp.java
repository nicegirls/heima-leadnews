package com.heima.article;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 韦东
 * @date 2021/8/30 周一
 */
@SpringBootApplication
@MapperScan("com.heima.article.mapper")
@ComponentScan({"com.heima.article", "com.heima.common", "com.heima.seata.config"})
@EnableFeignClients
@ServletComponentScan
public class ArticleApp {

    public static void main(String[] args) {
        SpringApplication.run(ArticleApp.class, args);
    }

    /**
     * mybatis-plus分页插件拦截器
     *
     * @return {@link PaginationInterceptor}
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
