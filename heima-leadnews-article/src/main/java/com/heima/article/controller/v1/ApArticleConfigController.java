package com.heima.article.controller.v1;

import com.heima.article.service.ApArticleConfigService;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/8 周三
 */
@RestController
@RequestMapping("api/v1/article_config")
public class ApArticleConfigController {

    @Resource
    private ApArticleConfigService apArticleConfigService;

    @PostMapping("save")
    public ResponseResult save(@RequestBody ApArticleConfig articleConfig) {
        if (articleConfig == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        this.apArticleConfigService.save(articleConfig);

        return ResponseResult.okResult(null);
    }
}
