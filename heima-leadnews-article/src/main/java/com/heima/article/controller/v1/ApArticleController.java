package com.heima.article.controller.v1;

import com.heima.article.service.ApArticleService;
import com.heima.model.article.pojos.ApArticle;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/8 周三
 */
@RestController
@RequestMapping("api/v1/article")
public class ApArticleController {

    @Resource
    private ApArticleService apArticleService;

    @PostMapping("save")
    public ApArticle save(@RequestBody ApArticle article) {
        if (article == null) {
            return null;
        }

        this.apArticleService.save(article);
        return article;
    }

    @GetMapping("one/{id}")
    public ApArticle findById(@PathVariable Long id) {
        return this.apArticleService.getById(id);
    }
}
