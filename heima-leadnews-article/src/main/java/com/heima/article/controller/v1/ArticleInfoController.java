package com.heima.article.controller.v1;

import com.heima.article.service.ArticleInfoService;
import com.heima.model.article.dtos.ArticleInfoDTO;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/10 周五
 */
@RestController
@RequestMapping("api/v1/article")
public class ArticleInfoController {

    @Resource
    private ArticleInfoService articleInfoService;

    @PostMapping("load_article_info")
    public ResponseResult loadArticleInfo(@RequestBody ArticleInfoDTO dto) {
        return this.articleInfoService.loadArticleInfo(dto);
    }

    @PostMapping("load_article_behavior")
    public ResponseResult loadArticleBehavior(@RequestBody ArticleInfoDTO dto) {
        return this.articleInfoService.loadArticleBehavior(dto);
    }
}
