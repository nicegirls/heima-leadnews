package com.heima.article.controller.v1;

import com.heima.article.service.ApCollectionService;
import com.heima.model.article.dtos.CollectionBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/12 周日
 */
@RestController
@RequestMapping("api/v1/collection_behavior")
public class ApCollectionController {

    @Resource
    private ApCollectionService apCollectionService;

    @PostMapping
    public ResponseResult collect(@RequestBody CollectionBehaviorDto dto) {
        return this.apCollectionService.collect(dto);
    }
}
