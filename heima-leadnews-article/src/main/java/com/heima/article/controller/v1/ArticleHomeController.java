package com.heima.article.controller.v1;

import com.heima.article.service.ApArticleService;
import com.heima.common.constants.ArticleConstans;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/10 周五
 */
@RestController
@RequestMapping("api/v1/article")
public class ArticleHomeController {
    @Resource
    private ApArticleService articleService;

    @PostMapping("load")
    public ResponseResult load(@RequestBody ArticleHomeDto dto) {
        return this.articleService.load(dto, ArticleConstans.LOADTYPE_LOAD_MORE);
    }

    @PostMapping("loadmore")
    public ResponseResult loadMore(@RequestBody ArticleHomeDto dto) {
        return this.articleService.load(dto, ArticleConstans.LOADTYPE_LOAD_MORE);
    }

    @PostMapping("loadnew")
    public ResponseResult loadNew(@RequestBody ArticleHomeDto dto) {
        return this.articleService.load(dto, ArticleConstans.LOADTYPE_LOAD_NEW);
    }
}
