package com.heima.article.controller.v1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.heima.article.service.ApAuthorService;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/2 周四
 */
@RestController
@RequestMapping("api/v1/author")
public class ApAuthorController {

    @Resource
    private ApAuthorService apAuthorService;

    @GetMapping("findByUserId/{userId}")
    public ResponseResult findByUserId(@PathVariable Integer userId){
        //检查参数
        if (userId == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //根据userId查询
        LambdaQueryWrapper<ApAuthor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ApAuthor::getUserId, userId);
        ApAuthor author = this.apAuthorService.getOne(queryWrapper);

        return ResponseResult.okResult(author);
    }

    @PostMapping("save")
    public ResponseResult save(@RequestBody ApAuthor author) {
        //检查参数
        if (author == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        this.apAuthorService.save(author);

        return ResponseResult.okResult(null);
    }

    @GetMapping("findByName/{name}")
    public ApAuthor findByName(@PathVariable String name) {
        return this.apAuthorService.findByName(name);
    }

    @GetMapping("one/{id}")
    public ApAuthor findById(@PathVariable Integer id) {
        return this.apAuthorService.getById(id);
    }
}
