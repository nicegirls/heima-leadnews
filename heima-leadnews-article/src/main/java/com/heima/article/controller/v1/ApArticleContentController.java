package com.heima.article.controller.v1;

import com.heima.article.service.ApArticleContentService;
import com.heima.model.article.pojos.ApArticleContent;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 韦东
 * @date 2021/9/8 周三
 */
@RestController
@RequestMapping("/api/v1/article_content")
public class ApArticleContentController {

    @Resource
    private ApArticleContentService apArticleContentService;

    @PostMapping("save")
    public ResponseResult save(@RequestBody ApArticleContent apArticleContent) {
        if (apArticleContent == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        this.apArticleContentService.save(apArticleContent);
        return ResponseResult.okResult(null);
    }
}
