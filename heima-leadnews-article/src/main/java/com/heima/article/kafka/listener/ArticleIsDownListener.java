package com.heima.article.kafka.listener;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.heima.article.service.ApArticleConfigService;
import com.heima.common.constants.WmNewsMessageConstants;
import com.heima.model.article.pojos.ApArticleConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 文章上下架监听器
 * 自媒体端上下架文章同步到app端
 *
 * @author 韦东
 * @date 2021-09-10
 */
@Component
public class ArticleIsDownListener {

    @Resource
    private ApArticleConfigService apArticleConfigService;

    @KafkaListener(topics = WmNewsMessageConstants.WM_NEWS_UP_OR_DOWN_TOPIC)
    public void receiveMessage(ConsumerRecord<?, ?> record) {
        if (record != null) {

            Object value = record.value();
            Map map = JSON.parseObject((String) value, Map.class);
            LambdaUpdateWrapper<ApArticleConfig> wrapper = new LambdaUpdateWrapper<>();
            wrapper.set(ApArticleConfig::getIsDown, map.get("enable")).eq(ApArticleConfig::getArticleId, map.get("articleId"));
            this.apArticleConfigService.update(wrapper);
        }
    }
}
