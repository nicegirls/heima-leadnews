package com.heima.article.feign;

import com.heima.model.behavior.dtos.BehaviorEntryDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApLikesBehavior;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 韦东
 * @date 2021/9/12 周日
 */
@FeignClient("leadnews-behavior")
public interface BehaviorFeign {

    @PostMapping("api/v1/behavior_entry")
    public ApBehaviorEntry findBehaviorEntryByUserIdOrEquipmentId(@RequestBody BehaviorEntryDto dto);

    @GetMapping("api/v1/likes_behavior/one")
    public ApLikesBehavior findLikeByArticleIdAndEntryId(
            @RequestParam("articleId") Long articleId,
            @RequestParam("entryId") Integer entryId,
            @RequestParam("type") Short type);

    @GetMapping("api/v1/unlike_behavior/one")
    public ApUnlikesBehavior findUnLikeByArticleIdAndEntryId(
            @RequestParam("articleId") Long articleId,
            @RequestParam("entryId") Integer entryId);

}
