package com.heima.article.service;

import com.heima.model.article.pojos.ApArticleContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface ApArticleContentService extends IService<ApArticleContent> {

}
