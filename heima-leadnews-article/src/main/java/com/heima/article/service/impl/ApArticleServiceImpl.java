package com.heima.article.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.ApArticleService;
import com.heima.common.constants.ArticleConstans;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 *
 */
@Service
public class ApArticleServiceImpl extends ServiceImpl<ApArticleMapper, ApArticle>
        implements ApArticleService {

    // 单页最大加载的数字
    private final static short MAX_PAGE_SIZE = 50;

    @Resource
    private ApArticleMapper apArticleMapper;

    @Value("${fdfs.url}")
    private String fileServiceUrl;

    @Override
    public ResponseResult load(ArticleHomeDto dto, Short loadType) {
        //校验参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        Integer size = dto.getSize();
        if (size == null || size == 0) {
            size = 10;
        }

        //最多查询50条数据
        size = Math.min(size, MAX_PAGE_SIZE);
        dto.setSize(size);
        //类型参数校验 默认加载更多
        if (!loadType.equals(ArticleConstans.LOADTYPE_LOAD_MORE) && !loadType.equals(ArticleConstans.LOADTYPE_LOAD_NEW)) {
            loadType = ArticleConstans.LOADTYPE_LOAD_MORE;
        }
        //文章评频道校验 不传默认所有频道
        String tag = dto.getTag();
        if (StringUtils.isBlank(tag)) {
            dto.setTag(ArticleConstans.DEFAULT_TAG);
        }

        //时间参数校验
        if (dto.getMaxBehotTime() == null) {
            dto.setMaxBehotTime(new Date());
        }
        if (dto.getMinBehotTime() == null) {

            dto.setMinBehotTime(new Date());
        }
        //查询数据
        List<ApArticle> list = this.apArticleMapper.loadArticleList(dto, loadType);

        ResponseResult result = ResponseResult.okResult(list);
        result.setHost(this.fileServiceUrl);

        return result;
    }
}




