package com.heima.article.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApAuthorMapper;
import com.heima.article.service.ApAuthorService;
import com.heima.model.article.pojos.ApAuthor;
import org.springframework.stereotype.Service;

/**
 * 作者服务impl
 *
 * @author 韦东
 * @date 2021-09-02
 */
@Service
public class ApAuthorServiceImpl extends ServiceImpl<ApAuthorMapper, ApAuthor>
        implements ApAuthorService {

    @Override
    public ApAuthor findByName(String name) {
        if (name == null) {
            return null;
        }

        LambdaQueryWrapper<ApAuthor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ApAuthor::getName, name);
        ApAuthor apAuthor = this.getOne(queryWrapper);

        return apAuthor;
    }
}




