package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.ResponseResult;

/**
 * 作者服务
 *
 * @author 韦东
 * @date 2021-09-02
 */
public interface ApAuthorService extends IService<ApAuthor> {

    /**
     * 根据名称查询作者信息
     *
     * @param name 的名字
     * @return {@link ResponseResult}
     */
    ApAuthor findByName(String name);

}
