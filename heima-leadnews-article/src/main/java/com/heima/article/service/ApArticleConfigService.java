package com.heima.article.service;

import com.heima.model.article.pojos.ApArticleConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface ApArticleConfigService extends IService<ApArticleConfig> {

}
