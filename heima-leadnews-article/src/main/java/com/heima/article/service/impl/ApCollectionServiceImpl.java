package com.heima.article.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.feign.BehaviorFeign;
import com.heima.article.mapper.ApCollectionMapper;
import com.heima.article.service.ApCollectionService;
import com.heima.model.article.dtos.CollectionBehaviorDto;
import com.heima.model.article.pojos.ApCollection;
import com.heima.model.behavior.dtos.BehaviorEntryDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 *
 */
@Service
public class ApCollectionServiceImpl extends ServiceImpl<ApCollectionMapper, ApCollection>
        implements ApCollectionService {

    @Resource
    private BehaviorFeign apBehaviorFeign;

    @Override
    public ResponseResult collect(CollectionBehaviorDto dto) {
        //检查参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询行为实体
        Integer userId = AppThreadLocalUtils.getUser().getId();
        BehaviorEntryDto entryDto = new BehaviorEntryDto();
        entryDto.setUserId(userId);
        entryDto.setEquipmentId(dto.getEquipmentId());
        ApBehaviorEntry behaviorEntry = this.apBehaviorFeign.findBehaviorEntryByUserIdOrEquipmentId(entryDto);
        if (behaviorEntry == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询收藏行为记录
        ApCollection apCollection = this.getOne(Wrappers.<ApCollection>lambdaQuery().eq(ApCollection::getArticleId, dto.getEntryId()).eq(ApCollection::getEntryId, behaviorEntry.getId()));
        //收藏记录存在 操作为收藏 执行收藏逻辑
        if (apCollection == null && dto.getOperation() == 0) {
            apCollection = new ApCollection();
            apCollection.setPublishedTime(dto.getPublishedTime());
            apCollection.setCollectionTime(new Date());
            apCollection.setType((short) 0);
            apCollection.setArticleId(dto.getEntryId());
            apCollection.setEntryId(behaviorEntry.getId());

            this.save(apCollection);
            return ResponseResult.okResult(null);
        }
        //收藏记录存在 操作为取消收藏 删除记录
        if (apCollection != null && dto.getOperation() == 1) {
            this.removeById(apCollection);
            return ResponseResult.okResult(null);
        }
        return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
    }
}





