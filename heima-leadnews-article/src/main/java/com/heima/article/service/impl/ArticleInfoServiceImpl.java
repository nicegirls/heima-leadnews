package com.heima.article.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.feign.BehaviorFeign;
import com.heima.article.feign.UserFeign;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.service.ApAuthorService;
import com.heima.article.service.ApCollectionService;
import com.heima.article.service.ArticleInfoService;
import com.heima.model.article.dtos.ArticleInfoDTO;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.article.pojos.ApArticleContent;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.article.pojos.ApCollection;
import com.heima.model.behavior.dtos.BehaviorEntryDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApLikesBehavior;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserFollow;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 韦东
 * @date 2021/9/10 周五
 */
@Service
public class ArticleInfoServiceImpl implements ArticleInfoService {

    @Resource
    private ApArticleConfigMapper apArticleConfigMapper;

    @Resource
    private ApArticleContentMapper apArticleContentMapper;

    @Resource
    private BehaviorFeign behaviorFeign;

    @Resource
    private ApCollectionService apCollectionService;
    @Resource
    private UserFeign userFeign;

    @Resource
    private ApAuthorService apAuthorService;

    @Override
    public ResponseResult loadArticleInfo(ArticleInfoDTO dto) {
        //检查参数
        if (dto == null || dto.getArticleId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        Map map = new HashMap();

        //查询配置
        ApArticleConfig articleConfig = this.apArticleConfigMapper.selectOne(Wrappers.<ApArticleConfig>lambdaQuery().eq(ApArticleConfig::getArticleId, dto.getArticleId()));
        if (articleConfig == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        if (!articleConfig.getIsDelete() && !articleConfig.getIsDown()) {
            ApArticleContent apArticleContent = this.apArticleContentMapper.selectOne(Wrappers.<ApArticleContent>lambdaQuery().eq(ApArticleContent::getArticleId, dto.getArticleId()));
            map.put("content", apArticleContent);
        }
        map.put("config", articleConfig);

        return ResponseResult.okResult(map);
    }

    @Override
    public ResponseResult loadArticleBehavior(ArticleInfoDTO dto) {
        //检查参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询行为实体
        ApUser apUser = AppThreadLocalUtils.getUser();
        Integer userId = apUser.getId();
        BehaviorEntryDto behaviorEntryDto = new BehaviorEntryDto();
        behaviorEntryDto.setUserId(userId);
        behaviorEntryDto.setEquipmentId(dto.getEquipmentId());
        ApBehaviorEntry behaviorEntry = this.behaviorFeign.findBehaviorEntryByUserIdOrEquipmentId(behaviorEntryDto);
        if (behaviorEntry == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        boolean isUnlike = false, isLike = false, isCollection = false, isFollow = false;

        //查询收藏行为
        ApCollection apCollection = this.apCollectionService.getOne(Wrappers.<ApCollection>lambdaQuery()
                .eq(ApCollection::getArticleId, dto.getArticleId())
                .eq(ApCollection::getEntryId, behaviorEntry.getId())
                .eq(ApCollection::getType, 0));
        if (apCollection != null) {
            isCollection = true;
        }
        //查询是否关注
        ApAuthor author = this.apAuthorService.getById(dto.getAuthorId());
        if (author != null) {
            ApUserFollow apUserFollow = this.userFeign.findByUserIdAndFollowId(userId, dto.getAuthorId());
            if (apUserFollow != null) {
                isFollow = true;
            }
        }
        //查询点赞行为
        ApLikesBehavior likesBehavior = this.behaviorFeign.findLikeByArticleIdAndEntryId(dto.getArticleId(), behaviorEntry.getId(), ApLikesBehavior.Type.ARTICLE.getCode());
        if (likesBehavior != null && likesBehavior.getOperation().equals(ApLikesBehavior.Operation.LIKE.getCode())) {
            isLike = true;
        }
        //查询不喜欢行为
        ApUnlikesBehavior unlikesBehavior = this.behaviorFeign.findUnLikeByArticleIdAndEntryId(dto.getArticleId(), behaviorEntry.getId());
        if (unlikesBehavior != null && unlikesBehavior.getType() == 0) {
            isUnlike = true;
        }

        Map<String, Boolean> map = new HashMap();
        map.put("isfollow", isFollow);
        map.put("islike", isLike);
        map.put("isunlike", isUnlike);
        map.put("iscollection", isCollection);
        return ResponseResult.okResult(map);
    }
}
