package com.heima.article.service;

import com.heima.model.article.dtos.ArticleInfoDTO;
import com.heima.model.common.dtos.ResponseResult;

/**
 * @author 韦东
 * @date 2021/9/10 周五
 */
public interface ArticleInfoService {

    /**
     * 加载文章信息
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    public ResponseResult loadArticleInfo(ArticleInfoDTO dto);

    /**
     * 加载文章行为信息
     *
     * @param dto dto
     * @return {@link ResponseResult}
     */
    ResponseResult loadArticleBehavior(ArticleInfoDTO dto);
}
