package com.heima.article.mapper;

import com.heima.model.article.pojos.ApCollection;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.heima.model.article.pojos.ApCollection
 */
public interface ApCollectionMapper extends BaseMapper<ApCollection> {

}




