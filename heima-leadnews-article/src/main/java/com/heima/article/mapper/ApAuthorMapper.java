package com.heima.article.mapper;

import com.heima.model.article.pojos.ApAuthor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.heima.model.article.pojos.ApAuthor
 */
public interface ApAuthorMapper extends BaseMapper<ApAuthor> {

}




