package com.heima.article.mapper;

import com.heima.model.article.pojos.ApArticleContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.heima.model.article.pojos.ApArticleContent
 */
public interface ApArticleContentMapper extends BaseMapper<ApArticleContent> {

}




