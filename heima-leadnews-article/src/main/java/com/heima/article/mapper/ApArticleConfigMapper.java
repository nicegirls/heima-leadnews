package com.heima.article.mapper;

import com.heima.model.article.pojos.ApArticleConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.heima.model.article.pojos.ApArticleConfig
 */
public interface ApArticleConfigMapper extends BaseMapper<ApArticleConfig> {

}




